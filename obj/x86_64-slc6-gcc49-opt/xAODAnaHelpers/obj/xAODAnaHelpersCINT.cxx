#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIafsdIcerndOchdIworkdIbdIbparidadIatlas_worksdIJetReconstructionHLLHCdIJetReconstructiondIRootCoreBindIobjdIx86_64mIslc6mIgcc49mIoptdIxAODAnaHelpersdIobjdIxAODAnaHelpersCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "xAODAnaHelpers/Algorithm.h"
#include "xAODAnaHelpers/BasicEventSelection.h"
#include "xAODAnaHelpers/ElectronSelector.h"
#include "xAODAnaHelpers/PhotonSelector.h"
#include "xAODAnaHelpers/TauSelector.h"
#include "xAODAnaHelpers/JetSelector.h"
#include "xAODAnaHelpers/DebugTool.h"
#include "xAODAnaHelpers/TruthSelector.h"
#include "xAODAnaHelpers/TrackSelector.h"
#include "xAODAnaHelpers/MuonSelector.h"
#include "xAODAnaHelpers/ElectronCalibrator.h"
#include "xAODAnaHelpers/PhotonCalibrator.h"
#include "xAODAnaHelpers/JetCalibrator.h"
#include "xAODAnaHelpers/MuonCalibrator.h"
#include "xAODAnaHelpers/HLTJetRoIBuilder.h"
#include "xAODAnaHelpers/HLTJetGetter.h"
#include "xAODAnaHelpers/METConstructor.h"
#include "xAODAnaHelpers/ElectronEfficiencyCorrector.h"
#include "xAODAnaHelpers/MuonEfficiencyCorrector.h"
#include "xAODAnaHelpers/BJetEfficiencyCorrector.h"
#include "xAODAnaHelpers/IParticleHistsAlgo.h"
#include "xAODAnaHelpers/JetHistsAlgo.h"
#include "xAODAnaHelpers/MuonHistsAlgo.h"
#include "xAODAnaHelpers/PhotonHistsAlgo.h"
#include "xAODAnaHelpers/ElectronHistsAlgo.h"
#include "xAODAnaHelpers/MetHistsAlgo.h"
#include "xAODAnaHelpers/TrackHistsAlgo.h"
#include "xAODAnaHelpers/ClusterHistsAlgo.h"
#include "xAODAnaHelpers/TreeAlgo.h"
#include "xAODAnaHelpers/MinixAOD.h"
#include "xAODAnaHelpers/OverlapRemover.h"
#include "xAODAnaHelpers/Writer.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_xAHcLcLAlgorithm(void *p = 0);
   static void *newArray_xAHcLcLAlgorithm(Long_t size, void *p);
   static void delete_xAHcLcLAlgorithm(void *p);
   static void deleteArray_xAHcLcLAlgorithm(void *p);
   static void destruct_xAHcLcLAlgorithm(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xAH::Algorithm*)
   {
      ::xAH::Algorithm *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::xAH::Algorithm >(0);
      static ::ROOT::TGenericClassInfo 
         instance("xAH::Algorithm", ::xAH::Algorithm::Class_Version(), "xAODAnaHelpers/Algorithm.h", 56,
                  typeid(::xAH::Algorithm), DefineBehavior(ptr, ptr),
                  &::xAH::Algorithm::Dictionary, isa_proxy, 4,
                  sizeof(::xAH::Algorithm) );
      instance.SetNew(&new_xAHcLcLAlgorithm);
      instance.SetNewArray(&newArray_xAHcLcLAlgorithm);
      instance.SetDelete(&delete_xAHcLcLAlgorithm);
      instance.SetDeleteArray(&deleteArray_xAHcLcLAlgorithm);
      instance.SetDestructor(&destruct_xAHcLcLAlgorithm);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xAH::Algorithm*)
   {
      return GenerateInitInstanceLocal((::xAH::Algorithm*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::xAH::Algorithm*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_BasicEventSelection(void *p = 0);
   static void *newArray_BasicEventSelection(Long_t size, void *p);
   static void delete_BasicEventSelection(void *p);
   static void deleteArray_BasicEventSelection(void *p);
   static void destruct_BasicEventSelection(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::BasicEventSelection*)
   {
      ::BasicEventSelection *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::BasicEventSelection >(0);
      static ::ROOT::TGenericClassInfo 
         instance("BasicEventSelection", ::BasicEventSelection::Class_Version(), "xAODAnaHelpers/BasicEventSelection.h", 33,
                  typeid(::BasicEventSelection), DefineBehavior(ptr, ptr),
                  &::BasicEventSelection::Dictionary, isa_proxy, 4,
                  sizeof(::BasicEventSelection) );
      instance.SetNew(&new_BasicEventSelection);
      instance.SetNewArray(&newArray_BasicEventSelection);
      instance.SetDelete(&delete_BasicEventSelection);
      instance.SetDeleteArray(&deleteArray_BasicEventSelection);
      instance.SetDestructor(&destruct_BasicEventSelection);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::BasicEventSelection*)
   {
      return GenerateInitInstanceLocal((::BasicEventSelection*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::BasicEventSelection*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_ElectronSelector(void *p = 0);
   static void *newArray_ElectronSelector(Long_t size, void *p);
   static void delete_ElectronSelector(void *p);
   static void deleteArray_ElectronSelector(void *p);
   static void destruct_ElectronSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElectronSelector*)
   {
      ::ElectronSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ElectronSelector >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ElectronSelector", ::ElectronSelector::Class_Version(), "xAODAnaHelpers/ElectronSelector.h", 33,
                  typeid(::ElectronSelector), DefineBehavior(ptr, ptr),
                  &::ElectronSelector::Dictionary, isa_proxy, 4,
                  sizeof(::ElectronSelector) );
      instance.SetNew(&new_ElectronSelector);
      instance.SetNewArray(&newArray_ElectronSelector);
      instance.SetDelete(&delete_ElectronSelector);
      instance.SetDeleteArray(&deleteArray_ElectronSelector);
      instance.SetDestructor(&destruct_ElectronSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElectronSelector*)
   {
      return GenerateInitInstanceLocal((::ElectronSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElectronSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_PhotonSelector(void *p = 0);
   static void *newArray_PhotonSelector(Long_t size, void *p);
   static void delete_PhotonSelector(void *p);
   static void deleteArray_PhotonSelector(void *p);
   static void destruct_PhotonSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PhotonSelector*)
   {
      ::PhotonSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::PhotonSelector >(0);
      static ::ROOT::TGenericClassInfo 
         instance("PhotonSelector", ::PhotonSelector::Class_Version(), "xAODAnaHelpers/PhotonSelector.h", 20,
                  typeid(::PhotonSelector), DefineBehavior(ptr, ptr),
                  &::PhotonSelector::Dictionary, isa_proxy, 4,
                  sizeof(::PhotonSelector) );
      instance.SetNew(&new_PhotonSelector);
      instance.SetNewArray(&newArray_PhotonSelector);
      instance.SetDelete(&delete_PhotonSelector);
      instance.SetDeleteArray(&deleteArray_PhotonSelector);
      instance.SetDestructor(&destruct_PhotonSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PhotonSelector*)
   {
      return GenerateInitInstanceLocal((::PhotonSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::PhotonSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TauSelector(void *p = 0);
   static void *newArray_TauSelector(Long_t size, void *p);
   static void delete_TauSelector(void *p);
   static void deleteArray_TauSelector(void *p);
   static void destruct_TauSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TauSelector*)
   {
      ::TauSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TauSelector >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TauSelector", ::TauSelector::Class_Version(), "xAODAnaHelpers/TauSelector.h", 18,
                  typeid(::TauSelector), DefineBehavior(ptr, ptr),
                  &::TauSelector::Dictionary, isa_proxy, 4,
                  sizeof(::TauSelector) );
      instance.SetNew(&new_TauSelector);
      instance.SetNewArray(&newArray_TauSelector);
      instance.SetDelete(&delete_TauSelector);
      instance.SetDeleteArray(&deleteArray_TauSelector);
      instance.SetDestructor(&destruct_TauSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TauSelector*)
   {
      return GenerateInitInstanceLocal((::TauSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TauSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_JetSelector(void *p = 0);
   static void *newArray_JetSelector(Long_t size, void *p);
   static void delete_JetSelector(void *p);
   static void deleteArray_JetSelector(void *p);
   static void destruct_JetSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetSelector*)
   {
      ::JetSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JetSelector >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JetSelector", ::JetSelector::Class_Version(), "xAODAnaHelpers/JetSelector.h", 30,
                  typeid(::JetSelector), DefineBehavior(ptr, ptr),
                  &::JetSelector::Dictionary, isa_proxy, 4,
                  sizeof(::JetSelector) );
      instance.SetNew(&new_JetSelector);
      instance.SetNewArray(&newArray_JetSelector);
      instance.SetDelete(&delete_JetSelector);
      instance.SetDeleteArray(&deleteArray_JetSelector);
      instance.SetDestructor(&destruct_JetSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetSelector*)
   {
      return GenerateInitInstanceLocal((::JetSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JetSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_DebugTool(void *p = 0);
   static void *newArray_DebugTool(Long_t size, void *p);
   static void delete_DebugTool(void *p);
   static void deleteArray_DebugTool(void *p);
   static void destruct_DebugTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DebugTool*)
   {
      ::DebugTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::DebugTool >(0);
      static ::ROOT::TGenericClassInfo 
         instance("DebugTool", ::DebugTool::Class_Version(), "xAODAnaHelpers/DebugTool.h", 8,
                  typeid(::DebugTool), DefineBehavior(ptr, ptr),
                  &::DebugTool::Dictionary, isa_proxy, 4,
                  sizeof(::DebugTool) );
      instance.SetNew(&new_DebugTool);
      instance.SetNewArray(&newArray_DebugTool);
      instance.SetDelete(&delete_DebugTool);
      instance.SetDeleteArray(&deleteArray_DebugTool);
      instance.SetDestructor(&destruct_DebugTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DebugTool*)
   {
      return GenerateInitInstanceLocal((::DebugTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DebugTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TruthSelector(void *p = 0);
   static void *newArray_TruthSelector(Long_t size, void *p);
   static void delete_TruthSelector(void *p);
   static void deleteArray_TruthSelector(void *p);
   static void destruct_TruthSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TruthSelector*)
   {
      ::TruthSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TruthSelector >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TruthSelector", ::TruthSelector::Class_Version(), "xAODAnaHelpers/TruthSelector.h", 17,
                  typeid(::TruthSelector), DefineBehavior(ptr, ptr),
                  &::TruthSelector::Dictionary, isa_proxy, 4,
                  sizeof(::TruthSelector) );
      instance.SetNew(&new_TruthSelector);
      instance.SetNewArray(&newArray_TruthSelector);
      instance.SetDelete(&delete_TruthSelector);
      instance.SetDeleteArray(&deleteArray_TruthSelector);
      instance.SetDestructor(&destruct_TruthSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TruthSelector*)
   {
      return GenerateInitInstanceLocal((::TruthSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TruthSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TrackSelector(void *p = 0);
   static void *newArray_TrackSelector(Long_t size, void *p);
   static void delete_TrackSelector(void *p);
   static void deleteArray_TrackSelector(void *p);
   static void destruct_TrackSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TrackSelector*)
   {
      ::TrackSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TrackSelector >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TrackSelector", ::TrackSelector::Class_Version(), "xAODAnaHelpers/TrackSelector.h", 13,
                  typeid(::TrackSelector), DefineBehavior(ptr, ptr),
                  &::TrackSelector::Dictionary, isa_proxy, 4,
                  sizeof(::TrackSelector) );
      instance.SetNew(&new_TrackSelector);
      instance.SetNewArray(&newArray_TrackSelector);
      instance.SetDelete(&delete_TrackSelector);
      instance.SetDeleteArray(&deleteArray_TrackSelector);
      instance.SetDestructor(&destruct_TrackSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TrackSelector*)
   {
      return GenerateInitInstanceLocal((::TrackSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TrackSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_MuonSelector(void *p = 0);
   static void *newArray_MuonSelector(Long_t size, void *p);
   static void delete_MuonSelector(void *p);
   static void deleteArray_MuonSelector(void *p);
   static void destruct_MuonSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MuonSelector*)
   {
      ::MuonSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::MuonSelector >(0);
      static ::ROOT::TGenericClassInfo 
         instance("MuonSelector", ::MuonSelector::Class_Version(), "xAODAnaHelpers/MuonSelector.h", 35,
                  typeid(::MuonSelector), DefineBehavior(ptr, ptr),
                  &::MuonSelector::Dictionary, isa_proxy, 4,
                  sizeof(::MuonSelector) );
      instance.SetNew(&new_MuonSelector);
      instance.SetNewArray(&newArray_MuonSelector);
      instance.SetDelete(&delete_MuonSelector);
      instance.SetDeleteArray(&deleteArray_MuonSelector);
      instance.SetDestructor(&destruct_MuonSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MuonSelector*)
   {
      return GenerateInitInstanceLocal((::MuonSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MuonSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_ElectronCalibrator(void *p = 0);
   static void *newArray_ElectronCalibrator(Long_t size, void *p);
   static void delete_ElectronCalibrator(void *p);
   static void deleteArray_ElectronCalibrator(void *p);
   static void destruct_ElectronCalibrator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElectronCalibrator*)
   {
      ::ElectronCalibrator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ElectronCalibrator >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ElectronCalibrator", ::ElectronCalibrator::Class_Version(), "xAODAnaHelpers/ElectronCalibrator.h", 18,
                  typeid(::ElectronCalibrator), DefineBehavior(ptr, ptr),
                  &::ElectronCalibrator::Dictionary, isa_proxy, 4,
                  sizeof(::ElectronCalibrator) );
      instance.SetNew(&new_ElectronCalibrator);
      instance.SetNewArray(&newArray_ElectronCalibrator);
      instance.SetDelete(&delete_ElectronCalibrator);
      instance.SetDeleteArray(&deleteArray_ElectronCalibrator);
      instance.SetDestructor(&destruct_ElectronCalibrator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElectronCalibrator*)
   {
      return GenerateInitInstanceLocal((::ElectronCalibrator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElectronCalibrator*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_PhotonCalibrator(void *p = 0);
   static void *newArray_PhotonCalibrator(Long_t size, void *p);
   static void delete_PhotonCalibrator(void *p);
   static void deleteArray_PhotonCalibrator(void *p);
   static void destruct_PhotonCalibrator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PhotonCalibrator*)
   {
      ::PhotonCalibrator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::PhotonCalibrator >(0);
      static ::ROOT::TGenericClassInfo 
         instance("PhotonCalibrator", ::PhotonCalibrator::Class_Version(), "xAODAnaHelpers/PhotonCalibrator.h", 26,
                  typeid(::PhotonCalibrator), DefineBehavior(ptr, ptr),
                  &::PhotonCalibrator::Dictionary, isa_proxy, 4,
                  sizeof(::PhotonCalibrator) );
      instance.SetNew(&new_PhotonCalibrator);
      instance.SetNewArray(&newArray_PhotonCalibrator);
      instance.SetDelete(&delete_PhotonCalibrator);
      instance.SetDeleteArray(&deleteArray_PhotonCalibrator);
      instance.SetDestructor(&destruct_PhotonCalibrator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PhotonCalibrator*)
   {
      return GenerateInitInstanceLocal((::PhotonCalibrator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::PhotonCalibrator*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_JetCalibrator(void *p = 0);
   static void *newArray_JetCalibrator(Long_t size, void *p);
   static void delete_JetCalibrator(void *p);
   static void deleteArray_JetCalibrator(void *p);
   static void destruct_JetCalibrator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetCalibrator*)
   {
      ::JetCalibrator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JetCalibrator >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JetCalibrator", ::JetCalibrator::Class_Version(), "xAODAnaHelpers/JetCalibrator.h", 29,
                  typeid(::JetCalibrator), DefineBehavior(ptr, ptr),
                  &::JetCalibrator::Dictionary, isa_proxy, 4,
                  sizeof(::JetCalibrator) );
      instance.SetNew(&new_JetCalibrator);
      instance.SetNewArray(&newArray_JetCalibrator);
      instance.SetDelete(&delete_JetCalibrator);
      instance.SetDeleteArray(&deleteArray_JetCalibrator);
      instance.SetDestructor(&destruct_JetCalibrator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetCalibrator*)
   {
      return GenerateInitInstanceLocal((::JetCalibrator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JetCalibrator*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HLTJetRoIBuilder(void *p = 0);
   static void *newArray_HLTJetRoIBuilder(Long_t size, void *p);
   static void delete_HLTJetRoIBuilder(void *p);
   static void deleteArray_HLTJetRoIBuilder(void *p);
   static void destruct_HLTJetRoIBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HLTJetRoIBuilder*)
   {
      ::HLTJetRoIBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HLTJetRoIBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HLTJetRoIBuilder", ::HLTJetRoIBuilder::Class_Version(), "xAODAnaHelpers/HLTJetRoIBuilder.h", 21,
                  typeid(::HLTJetRoIBuilder), DefineBehavior(ptr, ptr),
                  &::HLTJetRoIBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::HLTJetRoIBuilder) );
      instance.SetNew(&new_HLTJetRoIBuilder);
      instance.SetNewArray(&newArray_HLTJetRoIBuilder);
      instance.SetDelete(&delete_HLTJetRoIBuilder);
      instance.SetDeleteArray(&deleteArray_HLTJetRoIBuilder);
      instance.SetDestructor(&destruct_HLTJetRoIBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HLTJetRoIBuilder*)
   {
      return GenerateInitInstanceLocal((::HLTJetRoIBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::HLTJetRoIBuilder*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_MuonCalibrator(void *p = 0);
   static void *newArray_MuonCalibrator(Long_t size, void *p);
   static void delete_MuonCalibrator(void *p);
   static void deleteArray_MuonCalibrator(void *p);
   static void destruct_MuonCalibrator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MuonCalibrator*)
   {
      ::MuonCalibrator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::MuonCalibrator >(0);
      static ::ROOT::TGenericClassInfo 
         instance("MuonCalibrator", ::MuonCalibrator::Class_Version(), "xAODAnaHelpers/MuonCalibrator.h", 9,
                  typeid(::MuonCalibrator), DefineBehavior(ptr, ptr),
                  &::MuonCalibrator::Dictionary, isa_proxy, 4,
                  sizeof(::MuonCalibrator) );
      instance.SetNew(&new_MuonCalibrator);
      instance.SetNewArray(&newArray_MuonCalibrator);
      instance.SetDelete(&delete_MuonCalibrator);
      instance.SetDeleteArray(&deleteArray_MuonCalibrator);
      instance.SetDestructor(&destruct_MuonCalibrator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MuonCalibrator*)
   {
      return GenerateInitInstanceLocal((::MuonCalibrator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MuonCalibrator*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HLTJetGetter(void *p = 0);
   static void *newArray_HLTJetGetter(Long_t size, void *p);
   static void delete_HLTJetGetter(void *p);
   static void deleteArray_HLTJetGetter(void *p);
   static void destruct_HLTJetGetter(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HLTJetGetter*)
   {
      ::HLTJetGetter *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HLTJetGetter >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HLTJetGetter", ::HLTJetGetter::Class_Version(), "xAODAnaHelpers/HLTJetGetter.h", 29,
                  typeid(::HLTJetGetter), DefineBehavior(ptr, ptr),
                  &::HLTJetGetter::Dictionary, isa_proxy, 4,
                  sizeof(::HLTJetGetter) );
      instance.SetNew(&new_HLTJetGetter);
      instance.SetNewArray(&newArray_HLTJetGetter);
      instance.SetDelete(&delete_HLTJetGetter);
      instance.SetDeleteArray(&deleteArray_HLTJetGetter);
      instance.SetDestructor(&destruct_HLTJetGetter);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HLTJetGetter*)
   {
      return GenerateInitInstanceLocal((::HLTJetGetter*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::HLTJetGetter*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_METConstructor(void *p = 0);
   static void *newArray_METConstructor(Long_t size, void *p);
   static void delete_METConstructor(void *p);
   static void deleteArray_METConstructor(void *p);
   static void destruct_METConstructor(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::METConstructor*)
   {
      ::METConstructor *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::METConstructor >(0);
      static ::ROOT::TGenericClassInfo 
         instance("METConstructor", ::METConstructor::Class_Version(), "xAODAnaHelpers/METConstructor.h", 19,
                  typeid(::METConstructor), DefineBehavior(ptr, ptr),
                  &::METConstructor::Dictionary, isa_proxy, 4,
                  sizeof(::METConstructor) );
      instance.SetNew(&new_METConstructor);
      instance.SetNewArray(&newArray_METConstructor);
      instance.SetDelete(&delete_METConstructor);
      instance.SetDeleteArray(&deleteArray_METConstructor);
      instance.SetDestructor(&destruct_METConstructor);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::METConstructor*)
   {
      return GenerateInitInstanceLocal((::METConstructor*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::METConstructor*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_ElectronEfficiencyCorrector(void *p = 0);
   static void *newArray_ElectronEfficiencyCorrector(Long_t size, void *p);
   static void delete_ElectronEfficiencyCorrector(void *p);
   static void deleteArray_ElectronEfficiencyCorrector(void *p);
   static void destruct_ElectronEfficiencyCorrector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElectronEfficiencyCorrector*)
   {
      ::ElectronEfficiencyCorrector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ElectronEfficiencyCorrector >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ElectronEfficiencyCorrector", ::ElectronEfficiencyCorrector::Class_Version(), "xAODAnaHelpers/ElectronEfficiencyCorrector.h", 26,
                  typeid(::ElectronEfficiencyCorrector), DefineBehavior(ptr, ptr),
                  &::ElectronEfficiencyCorrector::Dictionary, isa_proxy, 4,
                  sizeof(::ElectronEfficiencyCorrector) );
      instance.SetNew(&new_ElectronEfficiencyCorrector);
      instance.SetNewArray(&newArray_ElectronEfficiencyCorrector);
      instance.SetDelete(&delete_ElectronEfficiencyCorrector);
      instance.SetDeleteArray(&deleteArray_ElectronEfficiencyCorrector);
      instance.SetDestructor(&destruct_ElectronEfficiencyCorrector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElectronEfficiencyCorrector*)
   {
      return GenerateInitInstanceLocal((::ElectronEfficiencyCorrector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElectronEfficiencyCorrector*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_MuonEfficiencyCorrector(void *p = 0);
   static void *newArray_MuonEfficiencyCorrector(Long_t size, void *p);
   static void delete_MuonEfficiencyCorrector(void *p);
   static void deleteArray_MuonEfficiencyCorrector(void *p);
   static void destruct_MuonEfficiencyCorrector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MuonEfficiencyCorrector*)
   {
      ::MuonEfficiencyCorrector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::MuonEfficiencyCorrector >(0);
      static ::ROOT::TGenericClassInfo 
         instance("MuonEfficiencyCorrector", ::MuonEfficiencyCorrector::Class_Version(), "xAODAnaHelpers/MuonEfficiencyCorrector.h", 29,
                  typeid(::MuonEfficiencyCorrector), DefineBehavior(ptr, ptr),
                  &::MuonEfficiencyCorrector::Dictionary, isa_proxy, 4,
                  sizeof(::MuonEfficiencyCorrector) );
      instance.SetNew(&new_MuonEfficiencyCorrector);
      instance.SetNewArray(&newArray_MuonEfficiencyCorrector);
      instance.SetDelete(&delete_MuonEfficiencyCorrector);
      instance.SetDeleteArray(&deleteArray_MuonEfficiencyCorrector);
      instance.SetDestructor(&destruct_MuonEfficiencyCorrector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MuonEfficiencyCorrector*)
   {
      return GenerateInitInstanceLocal((::MuonEfficiencyCorrector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MuonEfficiencyCorrector*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_BJetEfficiencyCorrector(void *p = 0);
   static void *newArray_BJetEfficiencyCorrector(Long_t size, void *p);
   static void delete_BJetEfficiencyCorrector(void *p);
   static void deleteArray_BJetEfficiencyCorrector(void *p);
   static void destruct_BJetEfficiencyCorrector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::BJetEfficiencyCorrector*)
   {
      ::BJetEfficiencyCorrector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::BJetEfficiencyCorrector >(0);
      static ::ROOT::TGenericClassInfo 
         instance("BJetEfficiencyCorrector", ::BJetEfficiencyCorrector::Class_Version(), "xAODAnaHelpers/BJetEfficiencyCorrector.h", 18,
                  typeid(::BJetEfficiencyCorrector), DefineBehavior(ptr, ptr),
                  &::BJetEfficiencyCorrector::Dictionary, isa_proxy, 4,
                  sizeof(::BJetEfficiencyCorrector) );
      instance.SetNew(&new_BJetEfficiencyCorrector);
      instance.SetNewArray(&newArray_BJetEfficiencyCorrector);
      instance.SetDelete(&delete_BJetEfficiencyCorrector);
      instance.SetDeleteArray(&deleteArray_BJetEfficiencyCorrector);
      instance.SetDestructor(&destruct_BJetEfficiencyCorrector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::BJetEfficiencyCorrector*)
   {
      return GenerateInitInstanceLocal((::BJetEfficiencyCorrector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::BJetEfficiencyCorrector*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_IParticleHistsAlgo(void *p = 0);
   static void *newArray_IParticleHistsAlgo(Long_t size, void *p);
   static void delete_IParticleHistsAlgo(void *p);
   static void deleteArray_IParticleHistsAlgo(void *p);
   static void destruct_IParticleHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::IParticleHistsAlgo*)
   {
      ::IParticleHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::IParticleHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("IParticleHistsAlgo", ::IParticleHistsAlgo::Class_Version(), "xAODAnaHelpers/IParticleHistsAlgo.h", 15,
                  typeid(::IParticleHistsAlgo), DefineBehavior(ptr, ptr),
                  &::IParticleHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::IParticleHistsAlgo) );
      instance.SetNew(&new_IParticleHistsAlgo);
      instance.SetNewArray(&newArray_IParticleHistsAlgo);
      instance.SetDelete(&delete_IParticleHistsAlgo);
      instance.SetDeleteArray(&deleteArray_IParticleHistsAlgo);
      instance.SetDestructor(&destruct_IParticleHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::IParticleHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::IParticleHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::IParticleHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_JetHistsAlgo(void *p = 0);
   static void *newArray_JetHistsAlgo(Long_t size, void *p);
   static void delete_JetHistsAlgo(void *p);
   static void deleteArray_JetHistsAlgo(void *p);
   static void destruct_JetHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetHistsAlgo*)
   {
      ::JetHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JetHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JetHistsAlgo", ::JetHistsAlgo::Class_Version(), "xAODAnaHelpers/JetHistsAlgo.h", 9,
                  typeid(::JetHistsAlgo), DefineBehavior(ptr, ptr),
                  &::JetHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::JetHistsAlgo) );
      instance.SetNew(&new_JetHistsAlgo);
      instance.SetNewArray(&newArray_JetHistsAlgo);
      instance.SetDelete(&delete_JetHistsAlgo);
      instance.SetDeleteArray(&deleteArray_JetHistsAlgo);
      instance.SetDestructor(&destruct_JetHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::JetHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JetHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_MuonHistsAlgo(void *p = 0);
   static void *newArray_MuonHistsAlgo(Long_t size, void *p);
   static void delete_MuonHistsAlgo(void *p);
   static void deleteArray_MuonHistsAlgo(void *p);
   static void destruct_MuonHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MuonHistsAlgo*)
   {
      ::MuonHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::MuonHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("MuonHistsAlgo", ::MuonHistsAlgo::Class_Version(), "xAODAnaHelpers/MuonHistsAlgo.h", 6,
                  typeid(::MuonHistsAlgo), DefineBehavior(ptr, ptr),
                  &::MuonHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::MuonHistsAlgo) );
      instance.SetNew(&new_MuonHistsAlgo);
      instance.SetNewArray(&newArray_MuonHistsAlgo);
      instance.SetDelete(&delete_MuonHistsAlgo);
      instance.SetDeleteArray(&deleteArray_MuonHistsAlgo);
      instance.SetDestructor(&destruct_MuonHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MuonHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::MuonHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MuonHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_PhotonHistsAlgo(void *p = 0);
   static void *newArray_PhotonHistsAlgo(Long_t size, void *p);
   static void delete_PhotonHistsAlgo(void *p);
   static void deleteArray_PhotonHistsAlgo(void *p);
   static void destruct_PhotonHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PhotonHistsAlgo*)
   {
      ::PhotonHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::PhotonHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("PhotonHistsAlgo", ::PhotonHistsAlgo::Class_Version(), "xAODAnaHelpers/PhotonHistsAlgo.h", 6,
                  typeid(::PhotonHistsAlgo), DefineBehavior(ptr, ptr),
                  &::PhotonHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::PhotonHistsAlgo) );
      instance.SetNew(&new_PhotonHistsAlgo);
      instance.SetNewArray(&newArray_PhotonHistsAlgo);
      instance.SetDelete(&delete_PhotonHistsAlgo);
      instance.SetDeleteArray(&deleteArray_PhotonHistsAlgo);
      instance.SetDestructor(&destruct_PhotonHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PhotonHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::PhotonHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::PhotonHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_ElectronHistsAlgo(void *p = 0);
   static void *newArray_ElectronHistsAlgo(Long_t size, void *p);
   static void delete_ElectronHistsAlgo(void *p);
   static void deleteArray_ElectronHistsAlgo(void *p);
   static void destruct_ElectronHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ElectronHistsAlgo*)
   {
      ::ElectronHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ElectronHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ElectronHistsAlgo", ::ElectronHistsAlgo::Class_Version(), "xAODAnaHelpers/ElectronHistsAlgo.h", 6,
                  typeid(::ElectronHistsAlgo), DefineBehavior(ptr, ptr),
                  &::ElectronHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::ElectronHistsAlgo) );
      instance.SetNew(&new_ElectronHistsAlgo);
      instance.SetNewArray(&newArray_ElectronHistsAlgo);
      instance.SetDelete(&delete_ElectronHistsAlgo);
      instance.SetDeleteArray(&deleteArray_ElectronHistsAlgo);
      instance.SetDestructor(&destruct_ElectronHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ElectronHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::ElectronHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ElectronHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_MetHistsAlgo(void *p = 0);
   static void *newArray_MetHistsAlgo(Long_t size, void *p);
   static void delete_MetHistsAlgo(void *p);
   static void deleteArray_MetHistsAlgo(void *p);
   static void destruct_MetHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MetHistsAlgo*)
   {
      ::MetHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::MetHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("MetHistsAlgo", ::MetHistsAlgo::Class_Version(), "xAODAnaHelpers/MetHistsAlgo.h", 9,
                  typeid(::MetHistsAlgo), DefineBehavior(ptr, ptr),
                  &::MetHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::MetHistsAlgo) );
      instance.SetNew(&new_MetHistsAlgo);
      instance.SetNewArray(&newArray_MetHistsAlgo);
      instance.SetDelete(&delete_MetHistsAlgo);
      instance.SetDeleteArray(&deleteArray_MetHistsAlgo);
      instance.SetDestructor(&destruct_MetHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MetHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::MetHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MetHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TrackHistsAlgo(void *p = 0);
   static void *newArray_TrackHistsAlgo(Long_t size, void *p);
   static void delete_TrackHistsAlgo(void *p);
   static void deleteArray_TrackHistsAlgo(void *p);
   static void destruct_TrackHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TrackHistsAlgo*)
   {
      ::TrackHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TrackHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TrackHistsAlgo", ::TrackHistsAlgo::Class_Version(), "xAODAnaHelpers/TrackHistsAlgo.h", 9,
                  typeid(::TrackHistsAlgo), DefineBehavior(ptr, ptr),
                  &::TrackHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::TrackHistsAlgo) );
      instance.SetNew(&new_TrackHistsAlgo);
      instance.SetNewArray(&newArray_TrackHistsAlgo);
      instance.SetDelete(&delete_TrackHistsAlgo);
      instance.SetDeleteArray(&deleteArray_TrackHistsAlgo);
      instance.SetDestructor(&destruct_TrackHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TrackHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::TrackHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TrackHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_ClusterHistsAlgo(void *p = 0);
   static void *newArray_ClusterHistsAlgo(Long_t size, void *p);
   static void delete_ClusterHistsAlgo(void *p);
   static void deleteArray_ClusterHistsAlgo(void *p);
   static void destruct_ClusterHistsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ClusterHistsAlgo*)
   {
      ::ClusterHistsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ClusterHistsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ClusterHistsAlgo", ::ClusterHistsAlgo::Class_Version(), "xAODAnaHelpers/ClusterHistsAlgo.h", 9,
                  typeid(::ClusterHistsAlgo), DefineBehavior(ptr, ptr),
                  &::ClusterHistsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::ClusterHistsAlgo) );
      instance.SetNew(&new_ClusterHistsAlgo);
      instance.SetNewArray(&newArray_ClusterHistsAlgo);
      instance.SetDelete(&delete_ClusterHistsAlgo);
      instance.SetDeleteArray(&deleteArray_ClusterHistsAlgo);
      instance.SetDestructor(&destruct_ClusterHistsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ClusterHistsAlgo*)
   {
      return GenerateInitInstanceLocal((::ClusterHistsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ClusterHistsAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TreeAlgo(void *p = 0);
   static void *newArray_TreeAlgo(Long_t size, void *p);
   static void delete_TreeAlgo(void *p);
   static void deleteArray_TreeAlgo(void *p);
   static void destruct_TreeAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TreeAlgo*)
   {
      ::TreeAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TreeAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TreeAlgo", ::TreeAlgo::Class_Version(), "xAODAnaHelpers/TreeAlgo.h", 11,
                  typeid(::TreeAlgo), DefineBehavior(ptr, ptr),
                  &::TreeAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::TreeAlgo) );
      instance.SetNew(&new_TreeAlgo);
      instance.SetNewArray(&newArray_TreeAlgo);
      instance.SetDelete(&delete_TreeAlgo);
      instance.SetDeleteArray(&deleteArray_TreeAlgo);
      instance.SetDestructor(&destruct_TreeAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TreeAlgo*)
   {
      return GenerateInitInstanceLocal((::TreeAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TreeAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_MinixAOD(void *p = 0);
   static void *newArray_MinixAOD(Long_t size, void *p);
   static void delete_MinixAOD(void *p);
   static void deleteArray_MinixAOD(void *p);
   static void destruct_MinixAOD(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MinixAOD*)
   {
      ::MinixAOD *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::MinixAOD >(0);
      static ::ROOT::TGenericClassInfo 
         instance("MinixAOD", ::MinixAOD::Class_Version(), "xAODAnaHelpers/MinixAOD.h", 44,
                  typeid(::MinixAOD), DefineBehavior(ptr, ptr),
                  &::MinixAOD::Dictionary, isa_proxy, 4,
                  sizeof(::MinixAOD) );
      instance.SetNew(&new_MinixAOD);
      instance.SetNewArray(&newArray_MinixAOD);
      instance.SetDelete(&delete_MinixAOD);
      instance.SetDeleteArray(&deleteArray_MinixAOD);
      instance.SetDestructor(&destruct_MinixAOD);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MinixAOD*)
   {
      return GenerateInitInstanceLocal((::MinixAOD*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MinixAOD*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_OverlapRemover(void *p = 0);
   static void *newArray_OverlapRemover(Long_t size, void *p);
   static void delete_OverlapRemover(void *p);
   static void deleteArray_OverlapRemover(void *p);
   static void destruct_OverlapRemover(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::OverlapRemover*)
   {
      ::OverlapRemover *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::OverlapRemover >(0);
      static ::ROOT::TGenericClassInfo 
         instance("OverlapRemover", ::OverlapRemover::Class_Version(), "xAODAnaHelpers/OverlapRemover.h", 75,
                  typeid(::OverlapRemover), DefineBehavior(ptr, ptr),
                  &::OverlapRemover::Dictionary, isa_proxy, 4,
                  sizeof(::OverlapRemover) );
      instance.SetNew(&new_OverlapRemover);
      instance.SetNewArray(&newArray_OverlapRemover);
      instance.SetDelete(&delete_OverlapRemover);
      instance.SetDeleteArray(&deleteArray_OverlapRemover);
      instance.SetDestructor(&destruct_OverlapRemover);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::OverlapRemover*)
   {
      return GenerateInitInstanceLocal((::OverlapRemover*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::OverlapRemover*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_Writer(void *p = 0);
   static void *newArray_Writer(Long_t size, void *p);
   static void delete_Writer(void *p);
   static void deleteArray_Writer(void *p);
   static void destruct_Writer(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Writer*)
   {
      ::Writer *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Writer >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Writer", ::Writer::Class_Version(), "xAODAnaHelpers/Writer.h", 7,
                  typeid(::Writer), DefineBehavior(ptr, ptr),
                  &::Writer::Dictionary, isa_proxy, 4,
                  sizeof(::Writer) );
      instance.SetNew(&new_Writer);
      instance.SetNewArray(&newArray_Writer);
      instance.SetDelete(&delete_Writer);
      instance.SetDeleteArray(&deleteArray_Writer);
      instance.SetDestructor(&destruct_Writer);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Writer*)
   {
      return GenerateInitInstanceLocal((::Writer*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Writer*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace xAH {
//______________________________________________________________________________
atomic_TClass_ptr Algorithm::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *Algorithm::Class_Name()
{
   return "xAH::Algorithm";
}

//______________________________________________________________________________
const char *Algorithm::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAH::Algorithm*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int Algorithm::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xAH::Algorithm*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Algorithm::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAH::Algorithm*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Algorithm::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xAH::Algorithm*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace xAH
//______________________________________________________________________________
atomic_TClass_ptr BasicEventSelection::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *BasicEventSelection::Class_Name()
{
   return "BasicEventSelection";
}

//______________________________________________________________________________
const char *BasicEventSelection::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::BasicEventSelection*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int BasicEventSelection::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::BasicEventSelection*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *BasicEventSelection::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::BasicEventSelection*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *BasicEventSelection::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::BasicEventSelection*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ElectronSelector::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ElectronSelector::Class_Name()
{
   return "ElectronSelector";
}

//______________________________________________________________________________
const char *ElectronSelector::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ElectronSelector*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ElectronSelector::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ElectronSelector*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ElectronSelector::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ElectronSelector*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ElectronSelector::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ElectronSelector*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr PhotonSelector::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *PhotonSelector::Class_Name()
{
   return "PhotonSelector";
}

//______________________________________________________________________________
const char *PhotonSelector::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::PhotonSelector*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int PhotonSelector::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::PhotonSelector*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *PhotonSelector::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::PhotonSelector*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *PhotonSelector::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::PhotonSelector*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TauSelector::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TauSelector::Class_Name()
{
   return "TauSelector";
}

//______________________________________________________________________________
const char *TauSelector::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TauSelector*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TauSelector::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TauSelector*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TauSelector::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TauSelector*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TauSelector::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TauSelector*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JetSelector::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JetSelector::Class_Name()
{
   return "JetSelector";
}

//______________________________________________________________________________
const char *JetSelector::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetSelector*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JetSelector::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetSelector*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JetSelector::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetSelector*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JetSelector::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetSelector*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr DebugTool::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *DebugTool::Class_Name()
{
   return "DebugTool";
}

//______________________________________________________________________________
const char *DebugTool::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::DebugTool*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int DebugTool::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::DebugTool*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *DebugTool::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::DebugTool*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *DebugTool::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::DebugTool*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TruthSelector::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TruthSelector::Class_Name()
{
   return "TruthSelector";
}

//______________________________________________________________________________
const char *TruthSelector::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TruthSelector*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TruthSelector::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TruthSelector*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TruthSelector::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TruthSelector*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TruthSelector::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TruthSelector*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TrackSelector::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TrackSelector::Class_Name()
{
   return "TrackSelector";
}

//______________________________________________________________________________
const char *TrackSelector::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TrackSelector*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TrackSelector::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TrackSelector*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TrackSelector::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TrackSelector*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TrackSelector::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TrackSelector*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr MuonSelector::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *MuonSelector::Class_Name()
{
   return "MuonSelector";
}

//______________________________________________________________________________
const char *MuonSelector::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MuonSelector*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int MuonSelector::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MuonSelector*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *MuonSelector::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MuonSelector*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *MuonSelector::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MuonSelector*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ElectronCalibrator::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ElectronCalibrator::Class_Name()
{
   return "ElectronCalibrator";
}

//______________________________________________________________________________
const char *ElectronCalibrator::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ElectronCalibrator*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ElectronCalibrator::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ElectronCalibrator*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ElectronCalibrator::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ElectronCalibrator*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ElectronCalibrator::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ElectronCalibrator*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr PhotonCalibrator::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *PhotonCalibrator::Class_Name()
{
   return "PhotonCalibrator";
}

//______________________________________________________________________________
const char *PhotonCalibrator::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::PhotonCalibrator*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int PhotonCalibrator::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::PhotonCalibrator*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *PhotonCalibrator::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::PhotonCalibrator*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *PhotonCalibrator::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::PhotonCalibrator*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JetCalibrator::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JetCalibrator::Class_Name()
{
   return "JetCalibrator";
}

//______________________________________________________________________________
const char *JetCalibrator::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetCalibrator*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JetCalibrator::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetCalibrator*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JetCalibrator::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetCalibrator*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JetCalibrator::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetCalibrator*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HLTJetRoIBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HLTJetRoIBuilder::Class_Name()
{
   return "HLTJetRoIBuilder";
}

//______________________________________________________________________________
const char *HLTJetRoIBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HLTJetRoIBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HLTJetRoIBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HLTJetRoIBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HLTJetRoIBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HLTJetRoIBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HLTJetRoIBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HLTJetRoIBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr MuonCalibrator::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *MuonCalibrator::Class_Name()
{
   return "MuonCalibrator";
}

//______________________________________________________________________________
const char *MuonCalibrator::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MuonCalibrator*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int MuonCalibrator::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MuonCalibrator*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *MuonCalibrator::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MuonCalibrator*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *MuonCalibrator::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MuonCalibrator*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HLTJetGetter::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HLTJetGetter::Class_Name()
{
   return "HLTJetGetter";
}

//______________________________________________________________________________
const char *HLTJetGetter::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HLTJetGetter*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HLTJetGetter::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HLTJetGetter*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HLTJetGetter::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HLTJetGetter*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HLTJetGetter::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HLTJetGetter*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr METConstructor::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *METConstructor::Class_Name()
{
   return "METConstructor";
}

//______________________________________________________________________________
const char *METConstructor::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::METConstructor*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int METConstructor::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::METConstructor*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *METConstructor::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::METConstructor*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *METConstructor::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::METConstructor*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ElectronEfficiencyCorrector::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ElectronEfficiencyCorrector::Class_Name()
{
   return "ElectronEfficiencyCorrector";
}

//______________________________________________________________________________
const char *ElectronEfficiencyCorrector::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ElectronEfficiencyCorrector*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ElectronEfficiencyCorrector::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ElectronEfficiencyCorrector*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ElectronEfficiencyCorrector::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ElectronEfficiencyCorrector*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ElectronEfficiencyCorrector::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ElectronEfficiencyCorrector*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr MuonEfficiencyCorrector::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *MuonEfficiencyCorrector::Class_Name()
{
   return "MuonEfficiencyCorrector";
}

//______________________________________________________________________________
const char *MuonEfficiencyCorrector::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MuonEfficiencyCorrector*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int MuonEfficiencyCorrector::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MuonEfficiencyCorrector*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *MuonEfficiencyCorrector::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MuonEfficiencyCorrector*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *MuonEfficiencyCorrector::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MuonEfficiencyCorrector*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr BJetEfficiencyCorrector::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *BJetEfficiencyCorrector::Class_Name()
{
   return "BJetEfficiencyCorrector";
}

//______________________________________________________________________________
const char *BJetEfficiencyCorrector::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::BJetEfficiencyCorrector*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int BJetEfficiencyCorrector::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::BJetEfficiencyCorrector*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *BJetEfficiencyCorrector::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::BJetEfficiencyCorrector*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *BJetEfficiencyCorrector::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::BJetEfficiencyCorrector*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr IParticleHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *IParticleHistsAlgo::Class_Name()
{
   return "IParticleHistsAlgo";
}

//______________________________________________________________________________
const char *IParticleHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::IParticleHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int IParticleHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::IParticleHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *IParticleHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::IParticleHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *IParticleHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::IParticleHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JetHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JetHistsAlgo::Class_Name()
{
   return "JetHistsAlgo";
}

//______________________________________________________________________________
const char *JetHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JetHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JetHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JetHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr MuonHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *MuonHistsAlgo::Class_Name()
{
   return "MuonHistsAlgo";
}

//______________________________________________________________________________
const char *MuonHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MuonHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int MuonHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MuonHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *MuonHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MuonHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *MuonHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MuonHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr PhotonHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *PhotonHistsAlgo::Class_Name()
{
   return "PhotonHistsAlgo";
}

//______________________________________________________________________________
const char *PhotonHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::PhotonHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int PhotonHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::PhotonHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *PhotonHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::PhotonHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *PhotonHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::PhotonHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ElectronHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ElectronHistsAlgo::Class_Name()
{
   return "ElectronHistsAlgo";
}

//______________________________________________________________________________
const char *ElectronHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ElectronHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ElectronHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ElectronHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ElectronHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ElectronHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ElectronHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ElectronHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr MetHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *MetHistsAlgo::Class_Name()
{
   return "MetHistsAlgo";
}

//______________________________________________________________________________
const char *MetHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MetHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int MetHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MetHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *MetHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MetHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *MetHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MetHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TrackHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TrackHistsAlgo::Class_Name()
{
   return "TrackHistsAlgo";
}

//______________________________________________________________________________
const char *TrackHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TrackHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TrackHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TrackHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TrackHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TrackHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TrackHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TrackHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ClusterHistsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ClusterHistsAlgo::Class_Name()
{
   return "ClusterHistsAlgo";
}

//______________________________________________________________________________
const char *ClusterHistsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ClusterHistsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ClusterHistsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ClusterHistsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ClusterHistsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ClusterHistsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ClusterHistsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ClusterHistsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TreeAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TreeAlgo::Class_Name()
{
   return "TreeAlgo";
}

//______________________________________________________________________________
const char *TreeAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TreeAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TreeAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TreeAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TreeAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TreeAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TreeAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TreeAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr MinixAOD::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *MinixAOD::Class_Name()
{
   return "MinixAOD";
}

//______________________________________________________________________________
const char *MinixAOD::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MinixAOD*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int MinixAOD::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::MinixAOD*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *MinixAOD::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MinixAOD*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *MinixAOD::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::MinixAOD*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr OverlapRemover::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *OverlapRemover::Class_Name()
{
   return "OverlapRemover";
}

//______________________________________________________________________________
const char *OverlapRemover::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::OverlapRemover*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int OverlapRemover::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::OverlapRemover*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *OverlapRemover::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::OverlapRemover*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *OverlapRemover::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::OverlapRemover*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr Writer::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *Writer::Class_Name()
{
   return "Writer";
}

//______________________________________________________________________________
const char *Writer::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Writer*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int Writer::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Writer*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Writer::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Writer*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Writer::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Writer*)0x0)->GetClass(); }
   return fgIsA;
}

namespace xAH {
//______________________________________________________________________________
void Algorithm::Streamer(TBuffer &R__b)
{
   // Stream an object of class xAH::Algorithm.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(xAH::Algorithm::Class(),this);
   } else {
      R__b.WriteClassBuffer(xAH::Algorithm::Class(),this);
   }
}

} // namespace xAH
namespace ROOT {
   // Wrappers around operator new
   static void *new_xAHcLcLAlgorithm(void *p) {
      return  p ? new(p) ::xAH::Algorithm : new ::xAH::Algorithm;
   }
   static void *newArray_xAHcLcLAlgorithm(Long_t nElements, void *p) {
      return p ? new(p) ::xAH::Algorithm[nElements] : new ::xAH::Algorithm[nElements];
   }
   // Wrapper around operator delete
   static void delete_xAHcLcLAlgorithm(void *p) {
      delete ((::xAH::Algorithm*)p);
   }
   static void deleteArray_xAHcLcLAlgorithm(void *p) {
      delete [] ((::xAH::Algorithm*)p);
   }
   static void destruct_xAHcLcLAlgorithm(void *p) {
      typedef ::xAH::Algorithm current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xAH::Algorithm

//______________________________________________________________________________
void BasicEventSelection::Streamer(TBuffer &R__b)
{
   // Stream an object of class BasicEventSelection.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(BasicEventSelection::Class(),this);
   } else {
      R__b.WriteClassBuffer(BasicEventSelection::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_BasicEventSelection(void *p) {
      return  p ? new(p) ::BasicEventSelection : new ::BasicEventSelection;
   }
   static void *newArray_BasicEventSelection(Long_t nElements, void *p) {
      return p ? new(p) ::BasicEventSelection[nElements] : new ::BasicEventSelection[nElements];
   }
   // Wrapper around operator delete
   static void delete_BasicEventSelection(void *p) {
      delete ((::BasicEventSelection*)p);
   }
   static void deleteArray_BasicEventSelection(void *p) {
      delete [] ((::BasicEventSelection*)p);
   }
   static void destruct_BasicEventSelection(void *p) {
      typedef ::BasicEventSelection current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::BasicEventSelection

//______________________________________________________________________________
void ElectronSelector::Streamer(TBuffer &R__b)
{
   // Stream an object of class ElectronSelector.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ElectronSelector::Class(),this);
   } else {
      R__b.WriteClassBuffer(ElectronSelector::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElectronSelector(void *p) {
      return  p ? new(p) ::ElectronSelector : new ::ElectronSelector;
   }
   static void *newArray_ElectronSelector(Long_t nElements, void *p) {
      return p ? new(p) ::ElectronSelector[nElements] : new ::ElectronSelector[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElectronSelector(void *p) {
      delete ((::ElectronSelector*)p);
   }
   static void deleteArray_ElectronSelector(void *p) {
      delete [] ((::ElectronSelector*)p);
   }
   static void destruct_ElectronSelector(void *p) {
      typedef ::ElectronSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElectronSelector

//______________________________________________________________________________
void PhotonSelector::Streamer(TBuffer &R__b)
{
   // Stream an object of class PhotonSelector.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(PhotonSelector::Class(),this);
   } else {
      R__b.WriteClassBuffer(PhotonSelector::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_PhotonSelector(void *p) {
      return  p ? new(p) ::PhotonSelector : new ::PhotonSelector;
   }
   static void *newArray_PhotonSelector(Long_t nElements, void *p) {
      return p ? new(p) ::PhotonSelector[nElements] : new ::PhotonSelector[nElements];
   }
   // Wrapper around operator delete
   static void delete_PhotonSelector(void *p) {
      delete ((::PhotonSelector*)p);
   }
   static void deleteArray_PhotonSelector(void *p) {
      delete [] ((::PhotonSelector*)p);
   }
   static void destruct_PhotonSelector(void *p) {
      typedef ::PhotonSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::PhotonSelector

//______________________________________________________________________________
void TauSelector::Streamer(TBuffer &R__b)
{
   // Stream an object of class TauSelector.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TauSelector::Class(),this);
   } else {
      R__b.WriteClassBuffer(TauSelector::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TauSelector(void *p) {
      return  p ? new(p) ::TauSelector : new ::TauSelector;
   }
   static void *newArray_TauSelector(Long_t nElements, void *p) {
      return p ? new(p) ::TauSelector[nElements] : new ::TauSelector[nElements];
   }
   // Wrapper around operator delete
   static void delete_TauSelector(void *p) {
      delete ((::TauSelector*)p);
   }
   static void deleteArray_TauSelector(void *p) {
      delete [] ((::TauSelector*)p);
   }
   static void destruct_TauSelector(void *p) {
      typedef ::TauSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TauSelector

//______________________________________________________________________________
void JetSelector::Streamer(TBuffer &R__b)
{
   // Stream an object of class JetSelector.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JetSelector::Class(),this);
   } else {
      R__b.WriteClassBuffer(JetSelector::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JetSelector(void *p) {
      return  p ? new(p) ::JetSelector : new ::JetSelector;
   }
   static void *newArray_JetSelector(Long_t nElements, void *p) {
      return p ? new(p) ::JetSelector[nElements] : new ::JetSelector[nElements];
   }
   // Wrapper around operator delete
   static void delete_JetSelector(void *p) {
      delete ((::JetSelector*)p);
   }
   static void deleteArray_JetSelector(void *p) {
      delete [] ((::JetSelector*)p);
   }
   static void destruct_JetSelector(void *p) {
      typedef ::JetSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetSelector

//______________________________________________________________________________
void DebugTool::Streamer(TBuffer &R__b)
{
   // Stream an object of class DebugTool.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(DebugTool::Class(),this);
   } else {
      R__b.WriteClassBuffer(DebugTool::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_DebugTool(void *p) {
      return  p ? new(p) ::DebugTool : new ::DebugTool;
   }
   static void *newArray_DebugTool(Long_t nElements, void *p) {
      return p ? new(p) ::DebugTool[nElements] : new ::DebugTool[nElements];
   }
   // Wrapper around operator delete
   static void delete_DebugTool(void *p) {
      delete ((::DebugTool*)p);
   }
   static void deleteArray_DebugTool(void *p) {
      delete [] ((::DebugTool*)p);
   }
   static void destruct_DebugTool(void *p) {
      typedef ::DebugTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DebugTool

//______________________________________________________________________________
void TruthSelector::Streamer(TBuffer &R__b)
{
   // Stream an object of class TruthSelector.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TruthSelector::Class(),this);
   } else {
      R__b.WriteClassBuffer(TruthSelector::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TruthSelector(void *p) {
      return  p ? new(p) ::TruthSelector : new ::TruthSelector;
   }
   static void *newArray_TruthSelector(Long_t nElements, void *p) {
      return p ? new(p) ::TruthSelector[nElements] : new ::TruthSelector[nElements];
   }
   // Wrapper around operator delete
   static void delete_TruthSelector(void *p) {
      delete ((::TruthSelector*)p);
   }
   static void deleteArray_TruthSelector(void *p) {
      delete [] ((::TruthSelector*)p);
   }
   static void destruct_TruthSelector(void *p) {
      typedef ::TruthSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TruthSelector

//______________________________________________________________________________
void TrackSelector::Streamer(TBuffer &R__b)
{
   // Stream an object of class TrackSelector.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TrackSelector::Class(),this);
   } else {
      R__b.WriteClassBuffer(TrackSelector::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TrackSelector(void *p) {
      return  p ? new(p) ::TrackSelector : new ::TrackSelector;
   }
   static void *newArray_TrackSelector(Long_t nElements, void *p) {
      return p ? new(p) ::TrackSelector[nElements] : new ::TrackSelector[nElements];
   }
   // Wrapper around operator delete
   static void delete_TrackSelector(void *p) {
      delete ((::TrackSelector*)p);
   }
   static void deleteArray_TrackSelector(void *p) {
      delete [] ((::TrackSelector*)p);
   }
   static void destruct_TrackSelector(void *p) {
      typedef ::TrackSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TrackSelector

//______________________________________________________________________________
void MuonSelector::Streamer(TBuffer &R__b)
{
   // Stream an object of class MuonSelector.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(MuonSelector::Class(),this);
   } else {
      R__b.WriteClassBuffer(MuonSelector::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_MuonSelector(void *p) {
      return  p ? new(p) ::MuonSelector : new ::MuonSelector;
   }
   static void *newArray_MuonSelector(Long_t nElements, void *p) {
      return p ? new(p) ::MuonSelector[nElements] : new ::MuonSelector[nElements];
   }
   // Wrapper around operator delete
   static void delete_MuonSelector(void *p) {
      delete ((::MuonSelector*)p);
   }
   static void deleteArray_MuonSelector(void *p) {
      delete [] ((::MuonSelector*)p);
   }
   static void destruct_MuonSelector(void *p) {
      typedef ::MuonSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MuonSelector

//______________________________________________________________________________
void ElectronCalibrator::Streamer(TBuffer &R__b)
{
   // Stream an object of class ElectronCalibrator.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ElectronCalibrator::Class(),this);
   } else {
      R__b.WriteClassBuffer(ElectronCalibrator::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElectronCalibrator(void *p) {
      return  p ? new(p) ::ElectronCalibrator : new ::ElectronCalibrator;
   }
   static void *newArray_ElectronCalibrator(Long_t nElements, void *p) {
      return p ? new(p) ::ElectronCalibrator[nElements] : new ::ElectronCalibrator[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElectronCalibrator(void *p) {
      delete ((::ElectronCalibrator*)p);
   }
   static void deleteArray_ElectronCalibrator(void *p) {
      delete [] ((::ElectronCalibrator*)p);
   }
   static void destruct_ElectronCalibrator(void *p) {
      typedef ::ElectronCalibrator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElectronCalibrator

//______________________________________________________________________________
void PhotonCalibrator::Streamer(TBuffer &R__b)
{
   // Stream an object of class PhotonCalibrator.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(PhotonCalibrator::Class(),this);
   } else {
      R__b.WriteClassBuffer(PhotonCalibrator::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_PhotonCalibrator(void *p) {
      return  p ? new(p) ::PhotonCalibrator : new ::PhotonCalibrator;
   }
   static void *newArray_PhotonCalibrator(Long_t nElements, void *p) {
      return p ? new(p) ::PhotonCalibrator[nElements] : new ::PhotonCalibrator[nElements];
   }
   // Wrapper around operator delete
   static void delete_PhotonCalibrator(void *p) {
      delete ((::PhotonCalibrator*)p);
   }
   static void deleteArray_PhotonCalibrator(void *p) {
      delete [] ((::PhotonCalibrator*)p);
   }
   static void destruct_PhotonCalibrator(void *p) {
      typedef ::PhotonCalibrator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::PhotonCalibrator

//______________________________________________________________________________
void JetCalibrator::Streamer(TBuffer &R__b)
{
   // Stream an object of class JetCalibrator.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JetCalibrator::Class(),this);
   } else {
      R__b.WriteClassBuffer(JetCalibrator::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JetCalibrator(void *p) {
      return  p ? new(p) ::JetCalibrator : new ::JetCalibrator;
   }
   static void *newArray_JetCalibrator(Long_t nElements, void *p) {
      return p ? new(p) ::JetCalibrator[nElements] : new ::JetCalibrator[nElements];
   }
   // Wrapper around operator delete
   static void delete_JetCalibrator(void *p) {
      delete ((::JetCalibrator*)p);
   }
   static void deleteArray_JetCalibrator(void *p) {
      delete [] ((::JetCalibrator*)p);
   }
   static void destruct_JetCalibrator(void *p) {
      typedef ::JetCalibrator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetCalibrator

//______________________________________________________________________________
void HLTJetRoIBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class HLTJetRoIBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HLTJetRoIBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(HLTJetRoIBuilder::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HLTJetRoIBuilder(void *p) {
      return  p ? new(p) ::HLTJetRoIBuilder : new ::HLTJetRoIBuilder;
   }
   static void *newArray_HLTJetRoIBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::HLTJetRoIBuilder[nElements] : new ::HLTJetRoIBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HLTJetRoIBuilder(void *p) {
      delete ((::HLTJetRoIBuilder*)p);
   }
   static void deleteArray_HLTJetRoIBuilder(void *p) {
      delete [] ((::HLTJetRoIBuilder*)p);
   }
   static void destruct_HLTJetRoIBuilder(void *p) {
      typedef ::HLTJetRoIBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HLTJetRoIBuilder

//______________________________________________________________________________
void MuonCalibrator::Streamer(TBuffer &R__b)
{
   // Stream an object of class MuonCalibrator.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(MuonCalibrator::Class(),this);
   } else {
      R__b.WriteClassBuffer(MuonCalibrator::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_MuonCalibrator(void *p) {
      return  p ? new(p) ::MuonCalibrator : new ::MuonCalibrator;
   }
   static void *newArray_MuonCalibrator(Long_t nElements, void *p) {
      return p ? new(p) ::MuonCalibrator[nElements] : new ::MuonCalibrator[nElements];
   }
   // Wrapper around operator delete
   static void delete_MuonCalibrator(void *p) {
      delete ((::MuonCalibrator*)p);
   }
   static void deleteArray_MuonCalibrator(void *p) {
      delete [] ((::MuonCalibrator*)p);
   }
   static void destruct_MuonCalibrator(void *p) {
      typedef ::MuonCalibrator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MuonCalibrator

//______________________________________________________________________________
void HLTJetGetter::Streamer(TBuffer &R__b)
{
   // Stream an object of class HLTJetGetter.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HLTJetGetter::Class(),this);
   } else {
      R__b.WriteClassBuffer(HLTJetGetter::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HLTJetGetter(void *p) {
      return  p ? new(p) ::HLTJetGetter : new ::HLTJetGetter;
   }
   static void *newArray_HLTJetGetter(Long_t nElements, void *p) {
      return p ? new(p) ::HLTJetGetter[nElements] : new ::HLTJetGetter[nElements];
   }
   // Wrapper around operator delete
   static void delete_HLTJetGetter(void *p) {
      delete ((::HLTJetGetter*)p);
   }
   static void deleteArray_HLTJetGetter(void *p) {
      delete [] ((::HLTJetGetter*)p);
   }
   static void destruct_HLTJetGetter(void *p) {
      typedef ::HLTJetGetter current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HLTJetGetter

//______________________________________________________________________________
void METConstructor::Streamer(TBuffer &R__b)
{
   // Stream an object of class METConstructor.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(METConstructor::Class(),this);
   } else {
      R__b.WriteClassBuffer(METConstructor::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_METConstructor(void *p) {
      return  p ? new(p) ::METConstructor : new ::METConstructor;
   }
   static void *newArray_METConstructor(Long_t nElements, void *p) {
      return p ? new(p) ::METConstructor[nElements] : new ::METConstructor[nElements];
   }
   // Wrapper around operator delete
   static void delete_METConstructor(void *p) {
      delete ((::METConstructor*)p);
   }
   static void deleteArray_METConstructor(void *p) {
      delete [] ((::METConstructor*)p);
   }
   static void destruct_METConstructor(void *p) {
      typedef ::METConstructor current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::METConstructor

//______________________________________________________________________________
void ElectronEfficiencyCorrector::Streamer(TBuffer &R__b)
{
   // Stream an object of class ElectronEfficiencyCorrector.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ElectronEfficiencyCorrector::Class(),this);
   } else {
      R__b.WriteClassBuffer(ElectronEfficiencyCorrector::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElectronEfficiencyCorrector(void *p) {
      return  p ? new(p) ::ElectronEfficiencyCorrector : new ::ElectronEfficiencyCorrector;
   }
   static void *newArray_ElectronEfficiencyCorrector(Long_t nElements, void *p) {
      return p ? new(p) ::ElectronEfficiencyCorrector[nElements] : new ::ElectronEfficiencyCorrector[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElectronEfficiencyCorrector(void *p) {
      delete ((::ElectronEfficiencyCorrector*)p);
   }
   static void deleteArray_ElectronEfficiencyCorrector(void *p) {
      delete [] ((::ElectronEfficiencyCorrector*)p);
   }
   static void destruct_ElectronEfficiencyCorrector(void *p) {
      typedef ::ElectronEfficiencyCorrector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElectronEfficiencyCorrector

//______________________________________________________________________________
void MuonEfficiencyCorrector::Streamer(TBuffer &R__b)
{
   // Stream an object of class MuonEfficiencyCorrector.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(MuonEfficiencyCorrector::Class(),this);
   } else {
      R__b.WriteClassBuffer(MuonEfficiencyCorrector::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_MuonEfficiencyCorrector(void *p) {
      return  p ? new(p) ::MuonEfficiencyCorrector : new ::MuonEfficiencyCorrector;
   }
   static void *newArray_MuonEfficiencyCorrector(Long_t nElements, void *p) {
      return p ? new(p) ::MuonEfficiencyCorrector[nElements] : new ::MuonEfficiencyCorrector[nElements];
   }
   // Wrapper around operator delete
   static void delete_MuonEfficiencyCorrector(void *p) {
      delete ((::MuonEfficiencyCorrector*)p);
   }
   static void deleteArray_MuonEfficiencyCorrector(void *p) {
      delete [] ((::MuonEfficiencyCorrector*)p);
   }
   static void destruct_MuonEfficiencyCorrector(void *p) {
      typedef ::MuonEfficiencyCorrector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MuonEfficiencyCorrector

//______________________________________________________________________________
void BJetEfficiencyCorrector::Streamer(TBuffer &R__b)
{
   // Stream an object of class BJetEfficiencyCorrector.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(BJetEfficiencyCorrector::Class(),this);
   } else {
      R__b.WriteClassBuffer(BJetEfficiencyCorrector::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_BJetEfficiencyCorrector(void *p) {
      return  p ? new(p) ::BJetEfficiencyCorrector : new ::BJetEfficiencyCorrector;
   }
   static void *newArray_BJetEfficiencyCorrector(Long_t nElements, void *p) {
      return p ? new(p) ::BJetEfficiencyCorrector[nElements] : new ::BJetEfficiencyCorrector[nElements];
   }
   // Wrapper around operator delete
   static void delete_BJetEfficiencyCorrector(void *p) {
      delete ((::BJetEfficiencyCorrector*)p);
   }
   static void deleteArray_BJetEfficiencyCorrector(void *p) {
      delete [] ((::BJetEfficiencyCorrector*)p);
   }
   static void destruct_BJetEfficiencyCorrector(void *p) {
      typedef ::BJetEfficiencyCorrector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::BJetEfficiencyCorrector

//______________________________________________________________________________
void IParticleHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class IParticleHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(IParticleHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(IParticleHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_IParticleHistsAlgo(void *p) {
      return  p ? new(p) ::IParticleHistsAlgo : new ::IParticleHistsAlgo;
   }
   static void *newArray_IParticleHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::IParticleHistsAlgo[nElements] : new ::IParticleHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_IParticleHistsAlgo(void *p) {
      delete ((::IParticleHistsAlgo*)p);
   }
   static void deleteArray_IParticleHistsAlgo(void *p) {
      delete [] ((::IParticleHistsAlgo*)p);
   }
   static void destruct_IParticleHistsAlgo(void *p) {
      typedef ::IParticleHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::IParticleHistsAlgo

//______________________________________________________________________________
void JetHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class JetHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JetHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(JetHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JetHistsAlgo(void *p) {
      return  p ? new(p) ::JetHistsAlgo : new ::JetHistsAlgo;
   }
   static void *newArray_JetHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::JetHistsAlgo[nElements] : new ::JetHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_JetHistsAlgo(void *p) {
      delete ((::JetHistsAlgo*)p);
   }
   static void deleteArray_JetHistsAlgo(void *p) {
      delete [] ((::JetHistsAlgo*)p);
   }
   static void destruct_JetHistsAlgo(void *p) {
      typedef ::JetHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetHistsAlgo

//______________________________________________________________________________
void MuonHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class MuonHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(MuonHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(MuonHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_MuonHistsAlgo(void *p) {
      return  p ? new(p) ::MuonHistsAlgo : new ::MuonHistsAlgo;
   }
   static void *newArray_MuonHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::MuonHistsAlgo[nElements] : new ::MuonHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_MuonHistsAlgo(void *p) {
      delete ((::MuonHistsAlgo*)p);
   }
   static void deleteArray_MuonHistsAlgo(void *p) {
      delete [] ((::MuonHistsAlgo*)p);
   }
   static void destruct_MuonHistsAlgo(void *p) {
      typedef ::MuonHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MuonHistsAlgo

//______________________________________________________________________________
void PhotonHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class PhotonHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(PhotonHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(PhotonHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_PhotonHistsAlgo(void *p) {
      return  p ? new(p) ::PhotonHistsAlgo : new ::PhotonHistsAlgo;
   }
   static void *newArray_PhotonHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::PhotonHistsAlgo[nElements] : new ::PhotonHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_PhotonHistsAlgo(void *p) {
      delete ((::PhotonHistsAlgo*)p);
   }
   static void deleteArray_PhotonHistsAlgo(void *p) {
      delete [] ((::PhotonHistsAlgo*)p);
   }
   static void destruct_PhotonHistsAlgo(void *p) {
      typedef ::PhotonHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::PhotonHistsAlgo

//______________________________________________________________________________
void ElectronHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class ElectronHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ElectronHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(ElectronHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ElectronHistsAlgo(void *p) {
      return  p ? new(p) ::ElectronHistsAlgo : new ::ElectronHistsAlgo;
   }
   static void *newArray_ElectronHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::ElectronHistsAlgo[nElements] : new ::ElectronHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_ElectronHistsAlgo(void *p) {
      delete ((::ElectronHistsAlgo*)p);
   }
   static void deleteArray_ElectronHistsAlgo(void *p) {
      delete [] ((::ElectronHistsAlgo*)p);
   }
   static void destruct_ElectronHistsAlgo(void *p) {
      typedef ::ElectronHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ElectronHistsAlgo

//______________________________________________________________________________
void MetHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class MetHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(MetHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(MetHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_MetHistsAlgo(void *p) {
      return  p ? new(p) ::MetHistsAlgo : new ::MetHistsAlgo;
   }
   static void *newArray_MetHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::MetHistsAlgo[nElements] : new ::MetHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_MetHistsAlgo(void *p) {
      delete ((::MetHistsAlgo*)p);
   }
   static void deleteArray_MetHistsAlgo(void *p) {
      delete [] ((::MetHistsAlgo*)p);
   }
   static void destruct_MetHistsAlgo(void *p) {
      typedef ::MetHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MetHistsAlgo

//______________________________________________________________________________
void TrackHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class TrackHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TrackHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(TrackHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TrackHistsAlgo(void *p) {
      return  p ? new(p) ::TrackHistsAlgo : new ::TrackHistsAlgo;
   }
   static void *newArray_TrackHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::TrackHistsAlgo[nElements] : new ::TrackHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_TrackHistsAlgo(void *p) {
      delete ((::TrackHistsAlgo*)p);
   }
   static void deleteArray_TrackHistsAlgo(void *p) {
      delete [] ((::TrackHistsAlgo*)p);
   }
   static void destruct_TrackHistsAlgo(void *p) {
      typedef ::TrackHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TrackHistsAlgo

//______________________________________________________________________________
void ClusterHistsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class ClusterHistsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ClusterHistsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(ClusterHistsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ClusterHistsAlgo(void *p) {
      return  p ? new(p) ::ClusterHistsAlgo : new ::ClusterHistsAlgo;
   }
   static void *newArray_ClusterHistsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::ClusterHistsAlgo[nElements] : new ::ClusterHistsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_ClusterHistsAlgo(void *p) {
      delete ((::ClusterHistsAlgo*)p);
   }
   static void deleteArray_ClusterHistsAlgo(void *p) {
      delete [] ((::ClusterHistsAlgo*)p);
   }
   static void destruct_ClusterHistsAlgo(void *p) {
      typedef ::ClusterHistsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ClusterHistsAlgo

//______________________________________________________________________________
void TreeAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class TreeAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TreeAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(TreeAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TreeAlgo(void *p) {
      return  p ? new(p) ::TreeAlgo : new ::TreeAlgo;
   }
   static void *newArray_TreeAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::TreeAlgo[nElements] : new ::TreeAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_TreeAlgo(void *p) {
      delete ((::TreeAlgo*)p);
   }
   static void deleteArray_TreeAlgo(void *p) {
      delete [] ((::TreeAlgo*)p);
   }
   static void destruct_TreeAlgo(void *p) {
      typedef ::TreeAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TreeAlgo

//______________________________________________________________________________
void MinixAOD::Streamer(TBuffer &R__b)
{
   // Stream an object of class MinixAOD.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(MinixAOD::Class(),this);
   } else {
      R__b.WriteClassBuffer(MinixAOD::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_MinixAOD(void *p) {
      return  p ? new(p) ::MinixAOD : new ::MinixAOD;
   }
   static void *newArray_MinixAOD(Long_t nElements, void *p) {
      return p ? new(p) ::MinixAOD[nElements] : new ::MinixAOD[nElements];
   }
   // Wrapper around operator delete
   static void delete_MinixAOD(void *p) {
      delete ((::MinixAOD*)p);
   }
   static void deleteArray_MinixAOD(void *p) {
      delete [] ((::MinixAOD*)p);
   }
   static void destruct_MinixAOD(void *p) {
      typedef ::MinixAOD current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MinixAOD

//______________________________________________________________________________
void OverlapRemover::Streamer(TBuffer &R__b)
{
   // Stream an object of class OverlapRemover.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(OverlapRemover::Class(),this);
   } else {
      R__b.WriteClassBuffer(OverlapRemover::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_OverlapRemover(void *p) {
      return  p ? new(p) ::OverlapRemover : new ::OverlapRemover;
   }
   static void *newArray_OverlapRemover(Long_t nElements, void *p) {
      return p ? new(p) ::OverlapRemover[nElements] : new ::OverlapRemover[nElements];
   }
   // Wrapper around operator delete
   static void delete_OverlapRemover(void *p) {
      delete ((::OverlapRemover*)p);
   }
   static void deleteArray_OverlapRemover(void *p) {
      delete [] ((::OverlapRemover*)p);
   }
   static void destruct_OverlapRemover(void *p) {
      typedef ::OverlapRemover current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::OverlapRemover

//______________________________________________________________________________
void Writer::Streamer(TBuffer &R__b)
{
   // Stream an object of class Writer.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Writer::Class(),this);
   } else {
      R__b.WriteClassBuffer(Writer::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_Writer(void *p) {
      return  p ? new(p) ::Writer : new ::Writer;
   }
   static void *newArray_Writer(Long_t nElements, void *p) {
      return p ? new(p) ::Writer[nElements] : new ::Writer[nElements];
   }
   // Wrapper around operator delete
   static void delete_Writer(void *p) {
      delete ((::Writer*)p);
   }
   static void deleteArray_Writer(void *p) {
      delete [] ((::Writer*)p);
   }
   static void destruct_Writer(void *p) {
      typedef ::Writer current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Writer

namespace ROOT {
   static TClass *vectorlEstringgR_Dictionary();
   static void vectorlEstringgR_TClassManip(TClass*);
   static void *new_vectorlEstringgR(void *p = 0);
   static void *newArray_vectorlEstringgR(Long_t size, void *p);
   static void delete_vectorlEstringgR(void *p);
   static void deleteArray_vectorlEstringgR(void *p);
   static void destruct_vectorlEstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<string>*)
   {
      vector<string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<string>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<string>", -2, "vector", 214,
                  typeid(vector<string>), DefineBehavior(ptr, ptr),
                  &vectorlEstringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<string>) );
      instance.SetNew(&new_vectorlEstringgR);
      instance.SetNewArray(&newArray_vectorlEstringgR);
      instance.SetDelete(&delete_vectorlEstringgR);
      instance.SetDeleteArray(&deleteArray_vectorlEstringgR);
      instance.SetDestructor(&destruct_vectorlEstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<string>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<string>*)0x0)->GetClass();
      vectorlEstringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEstringgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<string> : new vector<string>;
   }
   static void *newArray_vectorlEstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<string>[nElements] : new vector<string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEstringgR(void *p) {
      delete ((vector<string>*)p);
   }
   static void deleteArray_vectorlEstringgR(void *p) {
      delete [] ((vector<string>*)p);
   }
   static void destruct_vectorlEstringgR(void *p) {
      typedef vector<string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<string>

namespace ROOT {
   static TClass *vectorlEfloatgR_Dictionary();
   static void vectorlEfloatgR_TClassManip(TClass*);
   static void *new_vectorlEfloatgR(void *p = 0);
   static void *newArray_vectorlEfloatgR(Long_t size, void *p);
   static void delete_vectorlEfloatgR(void *p);
   static void deleteArray_vectorlEfloatgR(void *p);
   static void destruct_vectorlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<float>*)
   {
      vector<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<float>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<float>", -2, "vector", 214,
                  typeid(vector<float>), DefineBehavior(ptr, ptr),
                  &vectorlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<float>) );
      instance.SetNew(&new_vectorlEfloatgR);
      instance.SetNewArray(&newArray_vectorlEfloatgR);
      instance.SetDelete(&delete_vectorlEfloatgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfloatgR);
      instance.SetDestructor(&destruct_vectorlEfloatgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<float> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<float>*)0x0)->GetClass();
      vectorlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfloatgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<float> : new vector<float>;
   }
   static void *newArray_vectorlEfloatgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<float>[nElements] : new vector<float>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfloatgR(void *p) {
      delete ((vector<float>*)p);
   }
   static void deleteArray_vectorlEfloatgR(void *p) {
      delete [] ((vector<float>*)p);
   }
   static void destruct_vectorlEfloatgR(void *p) {
      typedef vector<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<float>

namespace ROOT {
   static TClass *vectorlETStringgR_Dictionary();
   static void vectorlETStringgR_TClassManip(TClass*);
   static void *new_vectorlETStringgR(void *p = 0);
   static void *newArray_vectorlETStringgR(Long_t size, void *p);
   static void delete_vectorlETStringgR(void *p);
   static void deleteArray_vectorlETStringgR(void *p);
   static void destruct_vectorlETStringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TString>*)
   {
      vector<TString> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TString>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TString>", -2, "vector", 214,
                  typeid(vector<TString>), DefineBehavior(ptr, ptr),
                  &vectorlETStringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<TString>) );
      instance.SetNew(&new_vectorlETStringgR);
      instance.SetNewArray(&newArray_vectorlETStringgR);
      instance.SetDelete(&delete_vectorlETStringgR);
      instance.SetDeleteArray(&deleteArray_vectorlETStringgR);
      instance.SetDestructor(&destruct_vectorlETStringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TString> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<TString>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETStringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TString>*)0x0)->GetClass();
      vectorlETStringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETStringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETStringgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<TString> : new vector<TString>;
   }
   static void *newArray_vectorlETStringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<TString>[nElements] : new vector<TString>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETStringgR(void *p) {
      delete ((vector<TString>*)p);
   }
   static void deleteArray_vectorlETStringgR(void *p) {
      delete [] ((vector<TString>*)p);
   }
   static void destruct_vectorlETStringgR(void *p) {
      typedef vector<TString> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TString>

namespace ROOT {
   static TClass *setlEpairlEunsignedsPintcOunsignedsPintgRsPgR_Dictionary();
   static void setlEpairlEunsignedsPintcOunsignedsPintgRsPgR_TClassManip(TClass*);
   static void *new_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR(void *p = 0);
   static void *newArray_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR(Long_t size, void *p);
   static void delete_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR(void *p);
   static void deleteArray_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR(void *p);
   static void destruct_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const set<pair<unsigned int,unsigned int> >*)
   {
      set<pair<unsigned int,unsigned int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(set<pair<unsigned int,unsigned int> >));
      static ::ROOT::TGenericClassInfo 
         instance("set<pair<unsigned int,unsigned int> >", -2, "set", 90,
                  typeid(set<pair<unsigned int,unsigned int> >), DefineBehavior(ptr, ptr),
                  &setlEpairlEunsignedsPintcOunsignedsPintgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(set<pair<unsigned int,unsigned int> >) );
      instance.SetNew(&new_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR);
      instance.SetNewArray(&newArray_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR);
      instance.SetDelete(&delete_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR);
      instance.SetDeleteArray(&deleteArray_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR);
      instance.SetDestructor(&destruct_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Insert< set<pair<unsigned int,unsigned int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const set<pair<unsigned int,unsigned int> >*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *setlEpairlEunsignedsPintcOunsignedsPintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const set<pair<unsigned int,unsigned int> >*)0x0)->GetClass();
      setlEpairlEunsignedsPintcOunsignedsPintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void setlEpairlEunsignedsPintcOunsignedsPintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) set<pair<unsigned int,unsigned int> > : new set<pair<unsigned int,unsigned int> >;
   }
   static void *newArray_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) set<pair<unsigned int,unsigned int> >[nElements] : new set<pair<unsigned int,unsigned int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR(void *p) {
      delete ((set<pair<unsigned int,unsigned int> >*)p);
   }
   static void deleteArray_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR(void *p) {
      delete [] ((set<pair<unsigned int,unsigned int> >*)p);
   }
   static void destruct_setlEpairlEunsignedsPintcOunsignedsPintgRsPgR(void *p) {
      typedef set<pair<unsigned int,unsigned int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class set<pair<unsigned int,unsigned int> >

namespace {
  void TriggerDictionaryInitialization_xAODAnaHelpersCINT_Impl() {
    static const char* headers[] = {
"xAODAnaHelpers/Algorithm.h",
"xAODAnaHelpers/BasicEventSelection.h",
"xAODAnaHelpers/ElectronSelector.h",
"xAODAnaHelpers/PhotonSelector.h",
"xAODAnaHelpers/TauSelector.h",
"xAODAnaHelpers/JetSelector.h",
"xAODAnaHelpers/DebugTool.h",
"xAODAnaHelpers/TruthSelector.h",
"xAODAnaHelpers/TrackSelector.h",
"xAODAnaHelpers/MuonSelector.h",
"xAODAnaHelpers/ElectronCalibrator.h",
"xAODAnaHelpers/PhotonCalibrator.h",
"xAODAnaHelpers/JetCalibrator.h",
"xAODAnaHelpers/MuonCalibrator.h",
"xAODAnaHelpers/HLTJetRoIBuilder.h",
"xAODAnaHelpers/HLTJetGetter.h",
"xAODAnaHelpers/METConstructor.h",
"xAODAnaHelpers/ElectronEfficiencyCorrector.h",
"xAODAnaHelpers/MuonEfficiencyCorrector.h",
"xAODAnaHelpers/BJetEfficiencyCorrector.h",
"xAODAnaHelpers/IParticleHistsAlgo.h",
"xAODAnaHelpers/JetHistsAlgo.h",
"xAODAnaHelpers/MuonHistsAlgo.h",
"xAODAnaHelpers/PhotonHistsAlgo.h",
"xAODAnaHelpers/ElectronHistsAlgo.h",
"xAODAnaHelpers/MetHistsAlgo.h",
"xAODAnaHelpers/TrackHistsAlgo.h",
"xAODAnaHelpers/ClusterHistsAlgo.h",
"xAODAnaHelpers/TreeAlgo.h",
"xAODAnaHelpers/MinixAOD.h",
"xAODAnaHelpers/OverlapRemover.h",
"xAODAnaHelpers/Writer.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root",
"/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xAH{class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  Algorithm;}
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  BasicEventSelection;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  ElectronSelector;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  PhotonSelector;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  TauSelector;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  JetSelector;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  DebugTool;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  TruthSelector;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  TrackSelector;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  MuonSelector;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  ElectronCalibrator;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  PhotonCalibrator;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  JetCalibrator;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  HLTJetRoIBuilder;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  MuonCalibrator;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  HLTJetGetter;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  METConstructor;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  ElectronEfficiencyCorrector;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  MuonEfficiencyCorrector;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  BJetEfficiencyCorrector;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  IParticleHistsAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  JetHistsAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  MuonHistsAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  PhotonHistsAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  ElectronHistsAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  MetHistsAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  TrackHistsAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  ClusterHistsAlgo;
class __attribute__((annotate(R"ATTRDUMP(!)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(!)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  TreeAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  MinixAOD;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  OverlapRemover;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/xAODAnaHelpers/Root/LinkDef.h")))  Writer;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 24
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725"
#endif
#ifndef ASG_TEST_FILE_DATA
  #define ASG_TEST_FILE_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7562/data15_13TeV.00284154.physics_Main.merge.AOD.r7562_p2521/AOD.07687819._000382.pool.root.1"
#endif
#ifndef ASG_TEST_FILE_MC
  #define ASG_TEST_FILE_MC "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1"
#endif
#ifndef ASG_TEST_FILE_MCAFII
  #define ASG_TEST_FILE_MCAFII ""
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "xAODAnaHelpers"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "xAODAnaHelpers/Algorithm.h"
#include "xAODAnaHelpers/BasicEventSelection.h"
#include "xAODAnaHelpers/ElectronSelector.h"
#include "xAODAnaHelpers/PhotonSelector.h"
#include "xAODAnaHelpers/TauSelector.h"
#include "xAODAnaHelpers/JetSelector.h"
#include "xAODAnaHelpers/DebugTool.h"
#include "xAODAnaHelpers/TruthSelector.h"
#include "xAODAnaHelpers/TrackSelector.h"
#include "xAODAnaHelpers/MuonSelector.h"
#include "xAODAnaHelpers/ElectronCalibrator.h"
#include "xAODAnaHelpers/PhotonCalibrator.h"
#include "xAODAnaHelpers/JetCalibrator.h"
#include "xAODAnaHelpers/MuonCalibrator.h"
#include "xAODAnaHelpers/HLTJetRoIBuilder.h"
#include "xAODAnaHelpers/HLTJetGetter.h"
#include "xAODAnaHelpers/METConstructor.h"
#include "xAODAnaHelpers/ElectronEfficiencyCorrector.h"
#include "xAODAnaHelpers/MuonEfficiencyCorrector.h"
#include "xAODAnaHelpers/BJetEfficiencyCorrector.h"
#include "xAODAnaHelpers/IParticleHistsAlgo.h"
#include "xAODAnaHelpers/JetHistsAlgo.h"
#include "xAODAnaHelpers/MuonHistsAlgo.h"
#include "xAODAnaHelpers/PhotonHistsAlgo.h"
#include "xAODAnaHelpers/ElectronHistsAlgo.h"
#include "xAODAnaHelpers/MetHistsAlgo.h"
#include "xAODAnaHelpers/TrackHistsAlgo.h"
#include "xAODAnaHelpers/ClusterHistsAlgo.h"
#include "xAODAnaHelpers/TreeAlgo.h"
#include "xAODAnaHelpers/MinixAOD.h"
#include "xAODAnaHelpers/OverlapRemover.h"
#include "xAODAnaHelpers/Writer.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"BJetEfficiencyCorrector", payloadCode, "@",
"BasicEventSelection", payloadCode, "@",
"ClusterHistsAlgo", payloadCode, "@",
"DebugTool", payloadCode, "@",
"ElectronCalibrator", payloadCode, "@",
"ElectronEfficiencyCorrector", payloadCode, "@",
"ElectronHistsAlgo", payloadCode, "@",
"ElectronSelector", payloadCode, "@",
"HLTJetGetter", payloadCode, "@",
"HLTJetRoIBuilder", payloadCode, "@",
"IParticleHistsAlgo", payloadCode, "@",
"JetCalibrator", payloadCode, "@",
"JetHistsAlgo", payloadCode, "@",
"JetSelector", payloadCode, "@",
"METConstructor", payloadCode, "@",
"MetHistsAlgo", payloadCode, "@",
"MinixAOD", payloadCode, "@",
"MuonCalibrator", payloadCode, "@",
"MuonEfficiencyCorrector", payloadCode, "@",
"MuonHistsAlgo", payloadCode, "@",
"MuonSelector", payloadCode, "@",
"OverlapRemover", payloadCode, "@",
"PhotonCalibrator", payloadCode, "@",
"PhotonHistsAlgo", payloadCode, "@",
"PhotonSelector", payloadCode, "@",
"TauSelector", payloadCode, "@",
"TrackHistsAlgo", payloadCode, "@",
"TrackSelector", payloadCode, "@",
"TreeAlgo", payloadCode, "@",
"TruthSelector", payloadCode, "@",
"Writer", payloadCode, "@",
"xAH::Algorithm", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("xAODAnaHelpersCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_xAODAnaHelpersCINT_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_xAODAnaHelpersCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_xAODAnaHelpersCINT() {
  TriggerDictionaryInitialization_xAODAnaHelpersCINT_Impl();
}
