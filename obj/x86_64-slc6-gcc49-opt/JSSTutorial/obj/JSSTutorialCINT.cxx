#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIafsdIcerndOchdIworkdIbdIbparidadIatlas_worksdIJetReconstructionHLLHCdIJetReconstructiondIRootCoreBindIobjdIx86_64mIslc6mIgcc49mIoptdIJSSTutorialdIobjdIJSSTutorialCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "JSSTutorial/JSSTutorialAlgo.h"
#include "JSSTutorial/JSSTutorialJetToolsAlgo.h"
#include "JSSTutorial/JSSTutorialPythonConfigAlgo.h"
#include "JSSTutorial/JetBuildingAlgs.h"
#include "JSSTutorial/TrackSelecToolHelper.h"
#include "JSSTutorial/ToolConfigHelper.h"
#include "JSSTutorial/JSSMinixAOD.h"
#include "JSSTutorial/JetGlobalOptionsAlgo.h"
#include "JSSTutorial/FixVertexAlgo.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_JSSTutorialAlgo(void *p = 0);
   static void *newArray_JSSTutorialAlgo(Long_t size, void *p);
   static void delete_JSSTutorialAlgo(void *p);
   static void deleteArray_JSSTutorialAlgo(void *p);
   static void destruct_JSSTutorialAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JSSTutorialAlgo*)
   {
      ::JSSTutorialAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JSSTutorialAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JSSTutorialAlgo", ::JSSTutorialAlgo::Class_Version(), "JSSTutorial/JSSTutorialAlgo.h", 72,
                  typeid(::JSSTutorialAlgo), DefineBehavior(ptr, ptr),
                  &::JSSTutorialAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::JSSTutorialAlgo) );
      instance.SetNew(&new_JSSTutorialAlgo);
      instance.SetNewArray(&newArray_JSSTutorialAlgo);
      instance.SetDelete(&delete_JSSTutorialAlgo);
      instance.SetDeleteArray(&deleteArray_JSSTutorialAlgo);
      instance.SetDestructor(&destruct_JSSTutorialAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JSSTutorialAlgo*)
   {
      return GenerateInitInstanceLocal((::JSSTutorialAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JSSTutorialAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_JSSTutorialJetToolsAlgo(void *p = 0);
   static void *newArray_JSSTutorialJetToolsAlgo(Long_t size, void *p);
   static void delete_JSSTutorialJetToolsAlgo(void *p);
   static void deleteArray_JSSTutorialJetToolsAlgo(void *p);
   static void destruct_JSSTutorialJetToolsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JSSTutorialJetToolsAlgo*)
   {
      ::JSSTutorialJetToolsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JSSTutorialJetToolsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JSSTutorialJetToolsAlgo", ::JSSTutorialJetToolsAlgo::Class_Version(), "JSSTutorial/JSSTutorialJetToolsAlgo.h", 33,
                  typeid(::JSSTutorialJetToolsAlgo), DefineBehavior(ptr, ptr),
                  &::JSSTutorialJetToolsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::JSSTutorialJetToolsAlgo) );
      instance.SetNew(&new_JSSTutorialJetToolsAlgo);
      instance.SetNewArray(&newArray_JSSTutorialJetToolsAlgo);
      instance.SetDelete(&delete_JSSTutorialJetToolsAlgo);
      instance.SetDeleteArray(&deleteArray_JSSTutorialJetToolsAlgo);
      instance.SetDestructor(&destruct_JSSTutorialJetToolsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JSSTutorialJetToolsAlgo*)
   {
      return GenerateInitInstanceLocal((::JSSTutorialJetToolsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JSSTutorialJetToolsAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_JSSTutorialPythonConfigAlgo(void *p = 0);
   static void *newArray_JSSTutorialPythonConfigAlgo(Long_t size, void *p);
   static void delete_JSSTutorialPythonConfigAlgo(void *p);
   static void deleteArray_JSSTutorialPythonConfigAlgo(void *p);
   static void destruct_JSSTutorialPythonConfigAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JSSTutorialPythonConfigAlgo*)
   {
      ::JSSTutorialPythonConfigAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JSSTutorialPythonConfigAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JSSTutorialPythonConfigAlgo", ::JSSTutorialPythonConfigAlgo::Class_Version(), "JSSTutorial/JSSTutorialPythonConfigAlgo.h", 21,
                  typeid(::JSSTutorialPythonConfigAlgo), DefineBehavior(ptr, ptr),
                  &::JSSTutorialPythonConfigAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::JSSTutorialPythonConfigAlgo) );
      instance.SetNew(&new_JSSTutorialPythonConfigAlgo);
      instance.SetNewArray(&newArray_JSSTutorialPythonConfigAlgo);
      instance.SetDelete(&delete_JSSTutorialPythonConfigAlgo);
      instance.SetDeleteArray(&deleteArray_JSSTutorialPythonConfigAlgo);
      instance.SetDestructor(&destruct_JSSTutorialPythonConfigAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JSSTutorialPythonConfigAlgo*)
   {
      return GenerateInitInstanceLocal((::JSSTutorialPythonConfigAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JSSTutorialPythonConfigAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_JetExecuteToolAlgo(void *p = 0);
   static void *newArray_JetExecuteToolAlgo(Long_t size, void *p);
   static void delete_JetExecuteToolAlgo(void *p);
   static void deleteArray_JetExecuteToolAlgo(void *p);
   static void destruct_JetExecuteToolAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetExecuteToolAlgo*)
   {
      ::JetExecuteToolAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JetExecuteToolAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JetExecuteToolAlgo", ::JetExecuteToolAlgo::Class_Version(), "JSSTutorial/JetBuildingAlgs.h", 17,
                  typeid(::JetExecuteToolAlgo), DefineBehavior(ptr, ptr),
                  &::JetExecuteToolAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::JetExecuteToolAlgo) );
      instance.SetNew(&new_JetExecuteToolAlgo);
      instance.SetNewArray(&newArray_JetExecuteToolAlgo);
      instance.SetDelete(&delete_JetExecuteToolAlgo);
      instance.SetDeleteArray(&deleteArray_JetExecuteToolAlgo);
      instance.SetDestructor(&destruct_JetExecuteToolAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetExecuteToolAlgo*)
   {
      return GenerateInitInstanceLocal((::JetExecuteToolAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JetExecuteToolAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_ToolWrapper(void *p = 0);
   static void *newArray_ToolWrapper(Long_t size, void *p);
   static void delete_ToolWrapper(void *p);
   static void deleteArray_ToolWrapper(void *p);
   static void destruct_ToolWrapper(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ToolWrapper*)
   {
      ::ToolWrapper *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::ToolWrapper >(0);
      static ::ROOT::TGenericClassInfo 
         instance("ToolWrapper", ::ToolWrapper::Class_Version(), "JSSTutorial/ToolConfigHelper.h", 9,
                  typeid(::ToolWrapper), DefineBehavior(ptr, ptr),
                  &::ToolWrapper::Dictionary, isa_proxy, 4,
                  sizeof(::ToolWrapper) );
      instance.SetNew(&new_ToolWrapper);
      instance.SetNewArray(&newArray_ToolWrapper);
      instance.SetDelete(&delete_ToolWrapper);
      instance.SetDeleteArray(&deleteArray_ToolWrapper);
      instance.SetDestructor(&destruct_ToolWrapper);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ToolWrapper*)
   {
      return GenerateInitInstanceLocal((::ToolWrapper*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ToolWrapper*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_JetRecToolAlgo(void *p = 0);
   static void *newArray_JetRecToolAlgo(Long_t size, void *p);
   static void delete_JetRecToolAlgo(void *p);
   static void deleteArray_JetRecToolAlgo(void *p);
   static void destruct_JetRecToolAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetRecToolAlgo*)
   {
      ::JetRecToolAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JetRecToolAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JetRecToolAlgo", ::JetRecToolAlgo::Class_Version(), "JSSTutorial/JetBuildingAlgs.h", 62,
                  typeid(::JetRecToolAlgo), DefineBehavior(ptr, ptr),
                  &::JetRecToolAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::JetRecToolAlgo) );
      instance.SetNew(&new_JetRecToolAlgo);
      instance.SetNewArray(&newArray_JetRecToolAlgo);
      instance.SetDelete(&delete_JetRecToolAlgo);
      instance.SetDeleteArray(&deleteArray_JetRecToolAlgo);
      instance.SetDestructor(&destruct_JetRecToolAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetRecToolAlgo*)
   {
      return GenerateInitInstanceLocal((::JetRecToolAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JetRecToolAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_JetConstitSeqAlgo(void *p = 0);
   static void *newArray_JetConstitSeqAlgo(Long_t size, void *p);
   static void delete_JetConstitSeqAlgo(void *p);
   static void deleteArray_JetConstitSeqAlgo(void *p);
   static void destruct_JetConstitSeqAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetConstitSeqAlgo*)
   {
      ::JetConstitSeqAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JetConstitSeqAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JetConstitSeqAlgo", ::JetConstitSeqAlgo::Class_Version(), "JSSTutorial/JetBuildingAlgs.h", 74,
                  typeid(::JetConstitSeqAlgo), DefineBehavior(ptr, ptr),
                  &::JetConstitSeqAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::JetConstitSeqAlgo) );
      instance.SetNew(&new_JetConstitSeqAlgo);
      instance.SetNewArray(&newArray_JetConstitSeqAlgo);
      instance.SetDelete(&delete_JetConstitSeqAlgo);
      instance.SetDeleteArray(&deleteArray_JetConstitSeqAlgo);
      instance.SetDestructor(&destruct_JetConstitSeqAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetConstitSeqAlgo*)
   {
      return GenerateInitInstanceLocal((::JetConstitSeqAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JetConstitSeqAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_JSSMinixAOD(void *p = 0);
   static void *newArray_JSSMinixAOD(Long_t size, void *p);
   static void delete_JSSMinixAOD(void *p);
   static void deleteArray_JSSMinixAOD(void *p);
   static void destruct_JSSMinixAOD(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JSSMinixAOD*)
   {
      ::JSSMinixAOD *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JSSMinixAOD >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JSSMinixAOD", ::JSSMinixAOD::Class_Version(), "JSSTutorial/JSSMinixAOD.h", 9,
                  typeid(::JSSMinixAOD), DefineBehavior(ptr, ptr),
                  &::JSSMinixAOD::Dictionary, isa_proxy, 4,
                  sizeof(::JSSMinixAOD) );
      instance.SetNew(&new_JSSMinixAOD);
      instance.SetNewArray(&newArray_JSSMinixAOD);
      instance.SetDelete(&delete_JSSMinixAOD);
      instance.SetDeleteArray(&deleteArray_JSSMinixAOD);
      instance.SetDestructor(&destruct_JSSMinixAOD);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JSSMinixAOD*)
   {
      return GenerateInitInstanceLocal((::JSSMinixAOD*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JSSMinixAOD*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_JetGlobalOptionsAlgo(void *p = 0);
   static void *newArray_JetGlobalOptionsAlgo(Long_t size, void *p);
   static void delete_JetGlobalOptionsAlgo(void *p);
   static void deleteArray_JetGlobalOptionsAlgo(void *p);
   static void destruct_JetGlobalOptionsAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetGlobalOptionsAlgo*)
   {
      ::JetGlobalOptionsAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JetGlobalOptionsAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("JetGlobalOptionsAlgo", ::JetGlobalOptionsAlgo::Class_Version(), "JSSTutorial/JetGlobalOptionsAlgo.h", 10,
                  typeid(::JetGlobalOptionsAlgo), DefineBehavior(ptr, ptr),
                  &::JetGlobalOptionsAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::JetGlobalOptionsAlgo) );
      instance.SetNew(&new_JetGlobalOptionsAlgo);
      instance.SetNewArray(&newArray_JetGlobalOptionsAlgo);
      instance.SetDelete(&delete_JetGlobalOptionsAlgo);
      instance.SetDeleteArray(&deleteArray_JetGlobalOptionsAlgo);
      instance.SetDestructor(&destruct_JetGlobalOptionsAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetGlobalOptionsAlgo*)
   {
      return GenerateInitInstanceLocal((::JetGlobalOptionsAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JetGlobalOptionsAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_FixVertexAlgo(void *p = 0);
   static void *newArray_FixVertexAlgo(Long_t size, void *p);
   static void delete_FixVertexAlgo(void *p);
   static void deleteArray_FixVertexAlgo(void *p);
   static void destruct_FixVertexAlgo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::FixVertexAlgo*)
   {
      ::FixVertexAlgo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::FixVertexAlgo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("FixVertexAlgo", ::FixVertexAlgo::Class_Version(), "JSSTutorial/FixVertexAlgo.h", 10,
                  typeid(::FixVertexAlgo), DefineBehavior(ptr, ptr),
                  &::FixVertexAlgo::Dictionary, isa_proxy, 4,
                  sizeof(::FixVertexAlgo) );
      instance.SetNew(&new_FixVertexAlgo);
      instance.SetNewArray(&newArray_FixVertexAlgo);
      instance.SetDelete(&delete_FixVertexAlgo);
      instance.SetDeleteArray(&deleteArray_FixVertexAlgo);
      instance.SetDestructor(&destruct_FixVertexAlgo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::FixVertexAlgo*)
   {
      return GenerateInitInstanceLocal((::FixVertexAlgo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::FixVertexAlgo*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr JSSTutorialAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JSSTutorialAlgo::Class_Name()
{
   return "JSSTutorialAlgo";
}

//______________________________________________________________________________
const char *JSSTutorialAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JSSTutorialAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JSSTutorialAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JSSTutorialAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JSSTutorialAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JSSTutorialAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JSSTutorialAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JSSTutorialAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JSSTutorialJetToolsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JSSTutorialJetToolsAlgo::Class_Name()
{
   return "JSSTutorialJetToolsAlgo";
}

//______________________________________________________________________________
const char *JSSTutorialJetToolsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JSSTutorialJetToolsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JSSTutorialJetToolsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JSSTutorialJetToolsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JSSTutorialJetToolsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JSSTutorialJetToolsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JSSTutorialJetToolsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JSSTutorialJetToolsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JSSTutorialPythonConfigAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JSSTutorialPythonConfigAlgo::Class_Name()
{
   return "JSSTutorialPythonConfigAlgo";
}

//______________________________________________________________________________
const char *JSSTutorialPythonConfigAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JSSTutorialPythonConfigAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JSSTutorialPythonConfigAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JSSTutorialPythonConfigAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JSSTutorialPythonConfigAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JSSTutorialPythonConfigAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JSSTutorialPythonConfigAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JSSTutorialPythonConfigAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JetExecuteToolAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JetExecuteToolAlgo::Class_Name()
{
   return "JetExecuteToolAlgo";
}

//______________________________________________________________________________
const char *JetExecuteToolAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetExecuteToolAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JetExecuteToolAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetExecuteToolAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JetExecuteToolAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetExecuteToolAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JetExecuteToolAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetExecuteToolAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr ToolWrapper::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *ToolWrapper::Class_Name()
{
   return "ToolWrapper";
}

//______________________________________________________________________________
const char *ToolWrapper::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ToolWrapper*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int ToolWrapper::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::ToolWrapper*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *ToolWrapper::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ToolWrapper*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *ToolWrapper::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::ToolWrapper*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JetRecToolAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JetRecToolAlgo::Class_Name()
{
   return "JetRecToolAlgo";
}

//______________________________________________________________________________
const char *JetRecToolAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetRecToolAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JetRecToolAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetRecToolAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JetRecToolAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetRecToolAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JetRecToolAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetRecToolAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JetConstitSeqAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JetConstitSeqAlgo::Class_Name()
{
   return "JetConstitSeqAlgo";
}

//______________________________________________________________________________
const char *JetConstitSeqAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetConstitSeqAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JetConstitSeqAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetConstitSeqAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JetConstitSeqAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetConstitSeqAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JetConstitSeqAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetConstitSeqAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JSSMinixAOD::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JSSMinixAOD::Class_Name()
{
   return "JSSMinixAOD";
}

//______________________________________________________________________________
const char *JSSMinixAOD::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JSSMinixAOD*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JSSMinixAOD::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JSSMinixAOD*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JSSMinixAOD::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JSSMinixAOD*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JSSMinixAOD::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JSSMinixAOD*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JetGlobalOptionsAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *JetGlobalOptionsAlgo::Class_Name()
{
   return "JetGlobalOptionsAlgo";
}

//______________________________________________________________________________
const char *JetGlobalOptionsAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetGlobalOptionsAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int JetGlobalOptionsAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JetGlobalOptionsAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JetGlobalOptionsAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetGlobalOptionsAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JetGlobalOptionsAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JetGlobalOptionsAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr FixVertexAlgo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *FixVertexAlgo::Class_Name()
{
   return "FixVertexAlgo";
}

//______________________________________________________________________________
const char *FixVertexAlgo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::FixVertexAlgo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int FixVertexAlgo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::FixVertexAlgo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *FixVertexAlgo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::FixVertexAlgo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *FixVertexAlgo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::FixVertexAlgo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void JSSTutorialAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class JSSTutorialAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JSSTutorialAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(JSSTutorialAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JSSTutorialAlgo(void *p) {
      return  p ? new(p) ::JSSTutorialAlgo : new ::JSSTutorialAlgo;
   }
   static void *newArray_JSSTutorialAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::JSSTutorialAlgo[nElements] : new ::JSSTutorialAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_JSSTutorialAlgo(void *p) {
      delete ((::JSSTutorialAlgo*)p);
   }
   static void deleteArray_JSSTutorialAlgo(void *p) {
      delete [] ((::JSSTutorialAlgo*)p);
   }
   static void destruct_JSSTutorialAlgo(void *p) {
      typedef ::JSSTutorialAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JSSTutorialAlgo

//______________________________________________________________________________
void JSSTutorialJetToolsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class JSSTutorialJetToolsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JSSTutorialJetToolsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(JSSTutorialJetToolsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JSSTutorialJetToolsAlgo(void *p) {
      return  p ? new(p) ::JSSTutorialJetToolsAlgo : new ::JSSTutorialJetToolsAlgo;
   }
   static void *newArray_JSSTutorialJetToolsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::JSSTutorialJetToolsAlgo[nElements] : new ::JSSTutorialJetToolsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_JSSTutorialJetToolsAlgo(void *p) {
      delete ((::JSSTutorialJetToolsAlgo*)p);
   }
   static void deleteArray_JSSTutorialJetToolsAlgo(void *p) {
      delete [] ((::JSSTutorialJetToolsAlgo*)p);
   }
   static void destruct_JSSTutorialJetToolsAlgo(void *p) {
      typedef ::JSSTutorialJetToolsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JSSTutorialJetToolsAlgo

//______________________________________________________________________________
void JSSTutorialPythonConfigAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class JSSTutorialPythonConfigAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JSSTutorialPythonConfigAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(JSSTutorialPythonConfigAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JSSTutorialPythonConfigAlgo(void *p) {
      return  p ? new(p) ::JSSTutorialPythonConfigAlgo : new ::JSSTutorialPythonConfigAlgo;
   }
   static void *newArray_JSSTutorialPythonConfigAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::JSSTutorialPythonConfigAlgo[nElements] : new ::JSSTutorialPythonConfigAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_JSSTutorialPythonConfigAlgo(void *p) {
      delete ((::JSSTutorialPythonConfigAlgo*)p);
   }
   static void deleteArray_JSSTutorialPythonConfigAlgo(void *p) {
      delete [] ((::JSSTutorialPythonConfigAlgo*)p);
   }
   static void destruct_JSSTutorialPythonConfigAlgo(void *p) {
      typedef ::JSSTutorialPythonConfigAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JSSTutorialPythonConfigAlgo

//______________________________________________________________________________
void JetExecuteToolAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class JetExecuteToolAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JetExecuteToolAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(JetExecuteToolAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JetExecuteToolAlgo(void *p) {
      return  p ? new(p) ::JetExecuteToolAlgo : new ::JetExecuteToolAlgo;
   }
   static void *newArray_JetExecuteToolAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::JetExecuteToolAlgo[nElements] : new ::JetExecuteToolAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_JetExecuteToolAlgo(void *p) {
      delete ((::JetExecuteToolAlgo*)p);
   }
   static void deleteArray_JetExecuteToolAlgo(void *p) {
      delete [] ((::JetExecuteToolAlgo*)p);
   }
   static void destruct_JetExecuteToolAlgo(void *p) {
      typedef ::JetExecuteToolAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetExecuteToolAlgo

//______________________________________________________________________________
void ToolWrapper::Streamer(TBuffer &R__b)
{
   // Stream an object of class ToolWrapper.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(ToolWrapper::Class(),this);
   } else {
      R__b.WriteClassBuffer(ToolWrapper::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_ToolWrapper(void *p) {
      return  p ? new(p) ::ToolWrapper : new ::ToolWrapper;
   }
   static void *newArray_ToolWrapper(Long_t nElements, void *p) {
      return p ? new(p) ::ToolWrapper[nElements] : new ::ToolWrapper[nElements];
   }
   // Wrapper around operator delete
   static void delete_ToolWrapper(void *p) {
      delete ((::ToolWrapper*)p);
   }
   static void deleteArray_ToolWrapper(void *p) {
      delete [] ((::ToolWrapper*)p);
   }
   static void destruct_ToolWrapper(void *p) {
      typedef ::ToolWrapper current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ToolWrapper

//______________________________________________________________________________
void JetRecToolAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class JetRecToolAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JetRecToolAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(JetRecToolAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JetRecToolAlgo(void *p) {
      return  p ? new(p) ::JetRecToolAlgo : new ::JetRecToolAlgo;
   }
   static void *newArray_JetRecToolAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::JetRecToolAlgo[nElements] : new ::JetRecToolAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_JetRecToolAlgo(void *p) {
      delete ((::JetRecToolAlgo*)p);
   }
   static void deleteArray_JetRecToolAlgo(void *p) {
      delete [] ((::JetRecToolAlgo*)p);
   }
   static void destruct_JetRecToolAlgo(void *p) {
      typedef ::JetRecToolAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetRecToolAlgo

//______________________________________________________________________________
void JetConstitSeqAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class JetConstitSeqAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JetConstitSeqAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(JetConstitSeqAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JetConstitSeqAlgo(void *p) {
      return  p ? new(p) ::JetConstitSeqAlgo : new ::JetConstitSeqAlgo;
   }
   static void *newArray_JetConstitSeqAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::JetConstitSeqAlgo[nElements] : new ::JetConstitSeqAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_JetConstitSeqAlgo(void *p) {
      delete ((::JetConstitSeqAlgo*)p);
   }
   static void deleteArray_JetConstitSeqAlgo(void *p) {
      delete [] ((::JetConstitSeqAlgo*)p);
   }
   static void destruct_JetConstitSeqAlgo(void *p) {
      typedef ::JetConstitSeqAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetConstitSeqAlgo

//______________________________________________________________________________
void JSSMinixAOD::Streamer(TBuffer &R__b)
{
   // Stream an object of class JSSMinixAOD.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JSSMinixAOD::Class(),this);
   } else {
      R__b.WriteClassBuffer(JSSMinixAOD::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JSSMinixAOD(void *p) {
      return  p ? new(p) ::JSSMinixAOD : new ::JSSMinixAOD;
   }
   static void *newArray_JSSMinixAOD(Long_t nElements, void *p) {
      return p ? new(p) ::JSSMinixAOD[nElements] : new ::JSSMinixAOD[nElements];
   }
   // Wrapper around operator delete
   static void delete_JSSMinixAOD(void *p) {
      delete ((::JSSMinixAOD*)p);
   }
   static void deleteArray_JSSMinixAOD(void *p) {
      delete [] ((::JSSMinixAOD*)p);
   }
   static void destruct_JSSMinixAOD(void *p) {
      typedef ::JSSMinixAOD current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JSSMinixAOD

//______________________________________________________________________________
void JetGlobalOptionsAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class JetGlobalOptionsAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JetGlobalOptionsAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(JetGlobalOptionsAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_JetGlobalOptionsAlgo(void *p) {
      return  p ? new(p) ::JetGlobalOptionsAlgo : new ::JetGlobalOptionsAlgo;
   }
   static void *newArray_JetGlobalOptionsAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::JetGlobalOptionsAlgo[nElements] : new ::JetGlobalOptionsAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_JetGlobalOptionsAlgo(void *p) {
      delete ((::JetGlobalOptionsAlgo*)p);
   }
   static void deleteArray_JetGlobalOptionsAlgo(void *p) {
      delete [] ((::JetGlobalOptionsAlgo*)p);
   }
   static void destruct_JetGlobalOptionsAlgo(void *p) {
      typedef ::JetGlobalOptionsAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetGlobalOptionsAlgo

//______________________________________________________________________________
void FixVertexAlgo::Streamer(TBuffer &R__b)
{
   // Stream an object of class FixVertexAlgo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(FixVertexAlgo::Class(),this);
   } else {
      R__b.WriteClassBuffer(FixVertexAlgo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_FixVertexAlgo(void *p) {
      return  p ? new(p) ::FixVertexAlgo : new ::FixVertexAlgo;
   }
   static void *newArray_FixVertexAlgo(Long_t nElements, void *p) {
      return p ? new(p) ::FixVertexAlgo[nElements] : new ::FixVertexAlgo[nElements];
   }
   // Wrapper around operator delete
   static void delete_FixVertexAlgo(void *p) {
      delete ((::FixVertexAlgo*)p);
   }
   static void deleteArray_FixVertexAlgo(void *p) {
      delete [] ((::FixVertexAlgo*)p);
   }
   static void destruct_FixVertexAlgo(void *p) {
      typedef ::FixVertexAlgo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::FixVertexAlgo

namespace ROOT {
   static TClass *vectorlEstringgR_Dictionary();
   static void vectorlEstringgR_TClassManip(TClass*);
   static void *new_vectorlEstringgR(void *p = 0);
   static void *newArray_vectorlEstringgR(Long_t size, void *p);
   static void delete_vectorlEstringgR(void *p);
   static void deleteArray_vectorlEstringgR(void *p);
   static void destruct_vectorlEstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<string>*)
   {
      vector<string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<string>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<string>", -2, "vector", 214,
                  typeid(vector<string>), DefineBehavior(ptr, ptr),
                  &vectorlEstringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<string>) );
      instance.SetNew(&new_vectorlEstringgR);
      instance.SetNewArray(&newArray_vectorlEstringgR);
      instance.SetDelete(&delete_vectorlEstringgR);
      instance.SetDeleteArray(&deleteArray_vectorlEstringgR);
      instance.SetDestructor(&destruct_vectorlEstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<string>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<string>*)0x0)->GetClass();
      vectorlEstringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEstringgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<string> : new vector<string>;
   }
   static void *newArray_vectorlEstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<string>[nElements] : new vector<string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEstringgR(void *p) {
      delete ((vector<string>*)p);
   }
   static void deleteArray_vectorlEstringgR(void *p) {
      delete [] ((vector<string>*)p);
   }
   static void destruct_vectorlEstringgR(void *p) {
      typedef vector<string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<string>

namespace {
  void TriggerDictionaryInitialization_JSSTutorialCINT_Impl() {
    static const char* headers[] = {
"JSSTutorial/JSSTutorialAlgo.h",
"JSSTutorial/JSSTutorialJetToolsAlgo.h",
"JSSTutorial/JSSTutorialPythonConfigAlgo.h",
"JSSTutorial/JetBuildingAlgs.h",
"JSSTutorial/TrackSelecToolHelper.h",
"JSSTutorial/ToolConfigHelper.h",
"JSSTutorial/JSSMinixAOD.h",
"JSSTutorial/JetGlobalOptionsAlgo.h",
"JSSTutorial/FixVertexAlgo.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JSSTutorial/Root",
"/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JSSTutorial",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JSSTutorial/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JSSTutorial/Root/LinkDef.h")))  JSSTutorialAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JSSTutorial/Root/LinkDef.h")))  JSSTutorialJetToolsAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JSSTutorial/Root/LinkDef.h")))  JSSTutorialPythonConfigAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JSSTutorial/Root/LinkDef.h")))  JetExecuteToolAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JSSTutorial/Root/LinkDef.h")))  ToolWrapper;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JSSTutorial/Root/LinkDef.h")))  JetRecToolAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JSSTutorial/Root/LinkDef.h")))  JetConstitSeqAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JSSTutorial/Root/LinkDef.h")))  JSSMinixAOD;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JSSTutorial/Root/LinkDef.h")))  JetGlobalOptionsAlgo;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JSSTutorial/Root/LinkDef.h")))  FixVertexAlgo;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 24
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725"
#endif
#ifndef ASG_TEST_FILE_DATA
  #define ASG_TEST_FILE_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7562/data15_13TeV.00284154.physics_Main.merge.AOD.r7562_p2521/AOD.07687819._000382.pool.root.1"
#endif
#ifndef ASG_TEST_FILE_MC
  #define ASG_TEST_FILE_MC "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1"
#endif
#ifndef ASG_TEST_FILE_MCAFII
  #define ASG_TEST_FILE_MCAFII ""
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "JSSTutorial"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "JSSTutorial/JSSTutorialAlgo.h"
#include "JSSTutorial/JSSTutorialJetToolsAlgo.h"
#include "JSSTutorial/JSSTutorialPythonConfigAlgo.h"
#include "JSSTutorial/JetBuildingAlgs.h"
#include "JSSTutorial/TrackSelecToolHelper.h"
#include "JSSTutorial/ToolConfigHelper.h"
#include "JSSTutorial/JSSMinixAOD.h"
#include "JSSTutorial/JetGlobalOptionsAlgo.h"
#include "JSSTutorial/FixVertexAlgo.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"FixVertexAlgo", payloadCode, "@",
"JSSMinixAOD", payloadCode, "@",
"JSSTutorialAlgo", payloadCode, "@",
"JSSTutorialJetToolsAlgo", payloadCode, "@",
"JSSTutorialPythonConfigAlgo", payloadCode, "@",
"JetConstitSeqAlgo", payloadCode, "@",
"JetExecuteToolAlgo", payloadCode, "@",
"JetGlobalOptionsAlgo", payloadCode, "@",
"JetRecToolAlgo", payloadCode, "@",
"ToolWrapper", payloadCode, "@",
"buildTightTrackVertexAssociationTool", payloadCode, "@",
"buildTrackSelectionTool", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("JSSTutorialCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_JSSTutorialCINT_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_JSSTutorialCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_JSSTutorialCINT() {
  TriggerDictionaryInitialization_JSSTutorialCINT_Impl();
}
