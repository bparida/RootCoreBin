#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIafsdIcerndOchdIworkdIbdIbparidadIatlas_worksdIJetReconstructionHLLHCdIJetReconstructiondIRootCoreBindIobjdIx86_64mIslc6mIgcc49mIoptdIJetRecToolsdIobjdIJetRecToolsCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "JetRecTools/JetTrackSelectionTool.h"
#include "JetRecTools/TrackVertexAssociationTool.h"
#include "JetRecTools/TrackPseudoJetGetter.h"
#include "JetRecTools/JetConstituentModSequence.h"
#include "JetRecTools/JetConstituentModifierBase.h"
#include "JetRecTools/CaloClusterConstituentsOrigin.h"
#include "JetRecTools/ConstituentSubtractorTool.h"
#include "JetRecTools/SoftKillerWeightTool.h"
#include "JetRecTools/VoronoiWeightTool.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *JetTrackSelectionTool_Dictionary();
   static void JetTrackSelectionTool_TClassManip(TClass*);
   static void delete_JetTrackSelectionTool(void *p);
   static void deleteArray_JetTrackSelectionTool(void *p);
   static void destruct_JetTrackSelectionTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetTrackSelectionTool*)
   {
      ::JetTrackSelectionTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::JetTrackSelectionTool));
      static ::ROOT::TGenericClassInfo 
         instance("JetTrackSelectionTool", "JetRecTools/JetTrackSelectionTool.h", 20,
                  typeid(::JetTrackSelectionTool), DefineBehavior(ptr, ptr),
                  &JetTrackSelectionTool_Dictionary, isa_proxy, 4,
                  sizeof(::JetTrackSelectionTool) );
      instance.SetDelete(&delete_JetTrackSelectionTool);
      instance.SetDeleteArray(&deleteArray_JetTrackSelectionTool);
      instance.SetDestructor(&destruct_JetTrackSelectionTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetTrackSelectionTool*)
   {
      return GenerateInitInstanceLocal((::JetTrackSelectionTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JetTrackSelectionTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *JetTrackSelectionTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::JetTrackSelectionTool*)0x0)->GetClass();
      JetTrackSelectionTool_TClassManip(theClass);
   return theClass;
   }

   static void JetTrackSelectionTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TrackVertexAssociationTool_Dictionary();
   static void TrackVertexAssociationTool_TClassManip(TClass*);
   static void delete_TrackVertexAssociationTool(void *p);
   static void deleteArray_TrackVertexAssociationTool(void *p);
   static void destruct_TrackVertexAssociationTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TrackVertexAssociationTool*)
   {
      ::TrackVertexAssociationTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TrackVertexAssociationTool));
      static ::ROOT::TGenericClassInfo 
         instance("TrackVertexAssociationTool", "JetRecTools/TrackVertexAssociationTool.h", 38,
                  typeid(::TrackVertexAssociationTool), DefineBehavior(ptr, ptr),
                  &TrackVertexAssociationTool_Dictionary, isa_proxy, 4,
                  sizeof(::TrackVertexAssociationTool) );
      instance.SetDelete(&delete_TrackVertexAssociationTool);
      instance.SetDeleteArray(&deleteArray_TrackVertexAssociationTool);
      instance.SetDestructor(&destruct_TrackVertexAssociationTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TrackVertexAssociationTool*)
   {
      return GenerateInitInstanceLocal((::TrackVertexAssociationTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TrackVertexAssociationTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TrackVertexAssociationTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TrackVertexAssociationTool*)0x0)->GetClass();
      TrackVertexAssociationTool_TClassManip(theClass);
   return theClass;
   }

   static void TrackVertexAssociationTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TrackPseudoJetGetter_Dictionary();
   static void TrackPseudoJetGetter_TClassManip(TClass*);
   static void delete_TrackPseudoJetGetter(void *p);
   static void deleteArray_TrackPseudoJetGetter(void *p);
   static void destruct_TrackPseudoJetGetter(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TrackPseudoJetGetter*)
   {
      ::TrackPseudoJetGetter *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TrackPseudoJetGetter));
      static ::ROOT::TGenericClassInfo 
         instance("TrackPseudoJetGetter", "JetRecTools/TrackPseudoJetGetter.h", 21,
                  typeid(::TrackPseudoJetGetter), DefineBehavior(ptr, ptr),
                  &TrackPseudoJetGetter_Dictionary, isa_proxy, 4,
                  sizeof(::TrackPseudoJetGetter) );
      instance.SetDelete(&delete_TrackPseudoJetGetter);
      instance.SetDeleteArray(&deleteArray_TrackPseudoJetGetter);
      instance.SetDestructor(&destruct_TrackPseudoJetGetter);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TrackPseudoJetGetter*)
   {
      return GenerateInitInstanceLocal((::TrackPseudoJetGetter*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TrackPseudoJetGetter*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TrackPseudoJetGetter_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TrackPseudoJetGetter*)0x0)->GetClass();
      TrackPseudoJetGetter_TClassManip(theClass);
   return theClass;
   }

   static void TrackPseudoJetGetter_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *JetConstituentModSequence_Dictionary();
   static void JetConstituentModSequence_TClassManip(TClass*);
   static void delete_JetConstituentModSequence(void *p);
   static void deleteArray_JetConstituentModSequence(void *p);
   static void destruct_JetConstituentModSequence(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetConstituentModSequence*)
   {
      ::JetConstituentModSequence *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::JetConstituentModSequence));
      static ::ROOT::TGenericClassInfo 
         instance("JetConstituentModSequence", "JetRecTools/JetConstituentModSequence.h", 19,
                  typeid(::JetConstituentModSequence), DefineBehavior(ptr, ptr),
                  &JetConstituentModSequence_Dictionary, isa_proxy, 4,
                  sizeof(::JetConstituentModSequence) );
      instance.SetDelete(&delete_JetConstituentModSequence);
      instance.SetDeleteArray(&deleteArray_JetConstituentModSequence);
      instance.SetDestructor(&destruct_JetConstituentModSequence);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetConstituentModSequence*)
   {
      return GenerateInitInstanceLocal((::JetConstituentModSequence*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JetConstituentModSequence*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *JetConstituentModSequence_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::JetConstituentModSequence*)0x0)->GetClass();
      JetConstituentModSequence_TClassManip(theClass);
   return theClass;
   }

   static void JetConstituentModSequence_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *JetConstituentModifierBase_Dictionary();
   static void JetConstituentModifierBase_TClassManip(TClass*);
   static void delete_JetConstituentModifierBase(void *p);
   static void deleteArray_JetConstituentModifierBase(void *p);
   static void destruct_JetConstituentModifierBase(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JetConstituentModifierBase*)
   {
      ::JetConstituentModifierBase *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::JetConstituentModifierBase));
      static ::ROOT::TGenericClassInfo 
         instance("JetConstituentModifierBase", "JetRecTools/JetConstituentModifierBase.h", 19,
                  typeid(::JetConstituentModifierBase), DefineBehavior(ptr, ptr),
                  &JetConstituentModifierBase_Dictionary, isa_proxy, 4,
                  sizeof(::JetConstituentModifierBase) );
      instance.SetDelete(&delete_JetConstituentModifierBase);
      instance.SetDeleteArray(&deleteArray_JetConstituentModifierBase);
      instance.SetDestructor(&destruct_JetConstituentModifierBase);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JetConstituentModifierBase*)
   {
      return GenerateInitInstanceLocal((::JetConstituentModifierBase*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::JetConstituentModifierBase*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *JetConstituentModifierBase_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::JetConstituentModifierBase*)0x0)->GetClass();
      JetConstituentModifierBase_TClassManip(theClass);
   return theClass;
   }

   static void JetConstituentModifierBase_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *CaloClusterConstituentsOrigin_Dictionary();
   static void CaloClusterConstituentsOrigin_TClassManip(TClass*);
   static void delete_CaloClusterConstituentsOrigin(void *p);
   static void deleteArray_CaloClusterConstituentsOrigin(void *p);
   static void destruct_CaloClusterConstituentsOrigin(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CaloClusterConstituentsOrigin*)
   {
      ::CaloClusterConstituentsOrigin *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::CaloClusterConstituentsOrigin));
      static ::ROOT::TGenericClassInfo 
         instance("CaloClusterConstituentsOrigin", "JetRecTools/CaloClusterConstituentsOrigin.h", 20,
                  typeid(::CaloClusterConstituentsOrigin), DefineBehavior(ptr, ptr),
                  &CaloClusterConstituentsOrigin_Dictionary, isa_proxy, 4,
                  sizeof(::CaloClusterConstituentsOrigin) );
      instance.SetDelete(&delete_CaloClusterConstituentsOrigin);
      instance.SetDeleteArray(&deleteArray_CaloClusterConstituentsOrigin);
      instance.SetDestructor(&destruct_CaloClusterConstituentsOrigin);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CaloClusterConstituentsOrigin*)
   {
      return GenerateInitInstanceLocal((::CaloClusterConstituentsOrigin*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::CaloClusterConstituentsOrigin*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CaloClusterConstituentsOrigin_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::CaloClusterConstituentsOrigin*)0x0)->GetClass();
      CaloClusterConstituentsOrigin_TClassManip(theClass);
   return theClass;
   }

   static void CaloClusterConstituentsOrigin_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ConstituentSubtractorTool_Dictionary();
   static void ConstituentSubtractorTool_TClassManip(TClass*);
   static void delete_ConstituentSubtractorTool(void *p);
   static void deleteArray_ConstituentSubtractorTool(void *p);
   static void destruct_ConstituentSubtractorTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ConstituentSubtractorTool*)
   {
      ::ConstituentSubtractorTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ConstituentSubtractorTool));
      static ::ROOT::TGenericClassInfo 
         instance("ConstituentSubtractorTool", "JetRecTools/ConstituentSubtractorTool.h", 18,
                  typeid(::ConstituentSubtractorTool), DefineBehavior(ptr, ptr),
                  &ConstituentSubtractorTool_Dictionary, isa_proxy, 4,
                  sizeof(::ConstituentSubtractorTool) );
      instance.SetDelete(&delete_ConstituentSubtractorTool);
      instance.SetDeleteArray(&deleteArray_ConstituentSubtractorTool);
      instance.SetDestructor(&destruct_ConstituentSubtractorTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ConstituentSubtractorTool*)
   {
      return GenerateInitInstanceLocal((::ConstituentSubtractorTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ConstituentSubtractorTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ConstituentSubtractorTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ConstituentSubtractorTool*)0x0)->GetClass();
      ConstituentSubtractorTool_TClassManip(theClass);
   return theClass;
   }

   static void ConstituentSubtractorTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SoftKillerWeightTool_Dictionary();
   static void SoftKillerWeightTool_TClassManip(TClass*);
   static void delete_SoftKillerWeightTool(void *p);
   static void deleteArray_SoftKillerWeightTool(void *p);
   static void destruct_SoftKillerWeightTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SoftKillerWeightTool*)
   {
      ::SoftKillerWeightTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::SoftKillerWeightTool));
      static ::ROOT::TGenericClassInfo 
         instance("SoftKillerWeightTool", "JetRecTools/SoftKillerWeightTool.h", 59,
                  typeid(::SoftKillerWeightTool), DefineBehavior(ptr, ptr),
                  &SoftKillerWeightTool_Dictionary, isa_proxy, 4,
                  sizeof(::SoftKillerWeightTool) );
      instance.SetDelete(&delete_SoftKillerWeightTool);
      instance.SetDeleteArray(&deleteArray_SoftKillerWeightTool);
      instance.SetDestructor(&destruct_SoftKillerWeightTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SoftKillerWeightTool*)
   {
      return GenerateInitInstanceLocal((::SoftKillerWeightTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SoftKillerWeightTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SoftKillerWeightTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SoftKillerWeightTool*)0x0)->GetClass();
      SoftKillerWeightTool_TClassManip(theClass);
   return theClass;
   }

   static void SoftKillerWeightTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *VoronoiWeightTool_Dictionary();
   static void VoronoiWeightTool_TClassManip(TClass*);
   static void delete_VoronoiWeightTool(void *p);
   static void deleteArray_VoronoiWeightTool(void *p);
   static void destruct_VoronoiWeightTool(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::VoronoiWeightTool*)
   {
      ::VoronoiWeightTool *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::VoronoiWeightTool));
      static ::ROOT::TGenericClassInfo 
         instance("VoronoiWeightTool", "JetRecTools/VoronoiWeightTool.h", 40,
                  typeid(::VoronoiWeightTool), DefineBehavior(ptr, ptr),
                  &VoronoiWeightTool_Dictionary, isa_proxy, 4,
                  sizeof(::VoronoiWeightTool) );
      instance.SetDelete(&delete_VoronoiWeightTool);
      instance.SetDeleteArray(&deleteArray_VoronoiWeightTool);
      instance.SetDestructor(&destruct_VoronoiWeightTool);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::VoronoiWeightTool*)
   {
      return GenerateInitInstanceLocal((::VoronoiWeightTool*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::VoronoiWeightTool*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *VoronoiWeightTool_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::VoronoiWeightTool*)0x0)->GetClass();
      VoronoiWeightTool_TClassManip(theClass);
   return theClass;
   }

   static void VoronoiWeightTool_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_JetTrackSelectionTool(void *p) {
      delete ((::JetTrackSelectionTool*)p);
   }
   static void deleteArray_JetTrackSelectionTool(void *p) {
      delete [] ((::JetTrackSelectionTool*)p);
   }
   static void destruct_JetTrackSelectionTool(void *p) {
      typedef ::JetTrackSelectionTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetTrackSelectionTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TrackVertexAssociationTool(void *p) {
      delete ((::TrackVertexAssociationTool*)p);
   }
   static void deleteArray_TrackVertexAssociationTool(void *p) {
      delete [] ((::TrackVertexAssociationTool*)p);
   }
   static void destruct_TrackVertexAssociationTool(void *p) {
      typedef ::TrackVertexAssociationTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TrackVertexAssociationTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TrackPseudoJetGetter(void *p) {
      delete ((::TrackPseudoJetGetter*)p);
   }
   static void deleteArray_TrackPseudoJetGetter(void *p) {
      delete [] ((::TrackPseudoJetGetter*)p);
   }
   static void destruct_TrackPseudoJetGetter(void *p) {
      typedef ::TrackPseudoJetGetter current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TrackPseudoJetGetter

namespace ROOT {
   // Wrapper around operator delete
   static void delete_JetConstituentModSequence(void *p) {
      delete ((::JetConstituentModSequence*)p);
   }
   static void deleteArray_JetConstituentModSequence(void *p) {
      delete [] ((::JetConstituentModSequence*)p);
   }
   static void destruct_JetConstituentModSequence(void *p) {
      typedef ::JetConstituentModSequence current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetConstituentModSequence

namespace ROOT {
   // Wrapper around operator delete
   static void delete_JetConstituentModifierBase(void *p) {
      delete ((::JetConstituentModifierBase*)p);
   }
   static void deleteArray_JetConstituentModifierBase(void *p) {
      delete [] ((::JetConstituentModifierBase*)p);
   }
   static void destruct_JetConstituentModifierBase(void *p) {
      typedef ::JetConstituentModifierBase current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::JetConstituentModifierBase

namespace ROOT {
   // Wrapper around operator delete
   static void delete_CaloClusterConstituentsOrigin(void *p) {
      delete ((::CaloClusterConstituentsOrigin*)p);
   }
   static void deleteArray_CaloClusterConstituentsOrigin(void *p) {
      delete [] ((::CaloClusterConstituentsOrigin*)p);
   }
   static void destruct_CaloClusterConstituentsOrigin(void *p) {
      typedef ::CaloClusterConstituentsOrigin current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::CaloClusterConstituentsOrigin

namespace ROOT {
   // Wrapper around operator delete
   static void delete_ConstituentSubtractorTool(void *p) {
      delete ((::ConstituentSubtractorTool*)p);
   }
   static void deleteArray_ConstituentSubtractorTool(void *p) {
      delete [] ((::ConstituentSubtractorTool*)p);
   }
   static void destruct_ConstituentSubtractorTool(void *p) {
      typedef ::ConstituentSubtractorTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ConstituentSubtractorTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SoftKillerWeightTool(void *p) {
      delete ((::SoftKillerWeightTool*)p);
   }
   static void deleteArray_SoftKillerWeightTool(void *p) {
      delete [] ((::SoftKillerWeightTool*)p);
   }
   static void destruct_SoftKillerWeightTool(void *p) {
      typedef ::SoftKillerWeightTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SoftKillerWeightTool

namespace ROOT {
   // Wrapper around operator delete
   static void delete_VoronoiWeightTool(void *p) {
      delete ((::VoronoiWeightTool*)p);
   }
   static void deleteArray_VoronoiWeightTool(void *p) {
      delete [] ((::VoronoiWeightTool*)p);
   }
   static void destruct_VoronoiWeightTool(void *p) {
      typedef ::VoronoiWeightTool current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::VoronoiWeightTool

namespace {
  void TriggerDictionaryInitialization_JetRecToolsCINT_Impl() {
    static const char* headers[] = {
"JetRecTools/JetTrackSelectionTool.h",
"JetRecTools/TrackVertexAssociationTool.h",
"JetRecTools/TrackPseudoJetGetter.h",
"JetRecTools/JetConstituentModSequence.h",
"JetRecTools/JetConstituentModifierBase.h",
"JetRecTools/CaloClusterConstituentsOrigin.h",
"JetRecTools/ConstituentSubtractorTool.h",
"JetRecTools/SoftKillerWeightTool.h",
"JetRecTools/VoronoiWeightTool.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JetRecTools/Root",
"/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JetRecTools",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JetRecTools/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JetRecTools/Root/LinkDef.h")))  JetTrackSelectionTool;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JetRecTools/Root/LinkDef.h")))  TrackVertexAssociationTool;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JetRecTools/Root/LinkDef.h")))  TrackPseudoJetGetter;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JetRecTools/Root/LinkDef.h")))  JetConstituentModSequence;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JetRecTools/Root/LinkDef.h")))  JetConstituentModifierBase;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JetRecTools/Root/LinkDef.h")))  CaloClusterConstituentsOrigin;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JetRecTools/Root/LinkDef.h")))  ConstituentSubtractorTool;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JetRecTools/Root/LinkDef.h")))  SoftKillerWeightTool;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/work/b/bparida/atlas_works/JetReconstructionHLLHC/JetReconstruction/JetRecTools/Root/LinkDef.h")))  VoronoiWeightTool;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 24
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725"
#endif
#ifndef ASG_TEST_FILE_DATA
  #define ASG_TEST_FILE_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7562/data15_13TeV.00284154.physics_Main.merge.AOD.r7562_p2521/AOD.07687819._000382.pool.root.1"
#endif
#ifndef ASG_TEST_FILE_MC
  #define ASG_TEST_FILE_MC "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1"
#endif
#ifndef ASG_TEST_FILE_MCAFII
  #define ASG_TEST_FILE_MCAFII ""
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "JetRecTools"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "JetRecTools/JetTrackSelectionTool.h"
#include "JetRecTools/TrackVertexAssociationTool.h"
#include "JetRecTools/TrackPseudoJetGetter.h"
#include "JetRecTools/JetConstituentModSequence.h"
#include "JetRecTools/JetConstituentModifierBase.h"
#include "JetRecTools/CaloClusterConstituentsOrigin.h"
#include "JetRecTools/ConstituentSubtractorTool.h"
#include "JetRecTools/SoftKillerWeightTool.h"
#include "JetRecTools/VoronoiWeightTool.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"CaloClusterConstituentsOrigin", payloadCode, "@",
"ConstituentSubtractorTool", payloadCode, "@",
"JetConstituentModSequence", payloadCode, "@",
"JetConstituentModifierBase", payloadCode, "@",
"JetTrackSelectionTool", payloadCode, "@",
"SoftKillerWeightTool", payloadCode, "@",
"TrackPseudoJetGetter", payloadCode, "@",
"TrackVertexAssociationTool", payloadCode, "@",
"VoronoiWeightTool", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("JetRecToolsCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_JetRecToolsCINT_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_JetRecToolsCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_JetRecToolsCINT() {
  TriggerDictionaryInitialization_JetRecToolsCINT_Impl();
}
